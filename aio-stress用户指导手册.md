# aio-stress使用文档

## 一、aio-stress简介

​	aio-stress用于检查O_DIRECT读写和缓冲之间一致性，读/写和截断的测试。

## 二、aio-stress安装

1. 进入openharmony的third_party目录,clone本仓和cyclictest的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/ltp.git
   ```

2. 进行外部配置改动

   vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "hispark_taurus_standard",
     "device_company": "hisilicon",
     "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "hispark_taurus",
     "enable_ramdisk": true,
     "subsystems": [
   #############################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   #############################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   vendor/hihope/rk3568/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "device_company": "rockchip",
     "device_build_path": "device/board/hihope/rk3568",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "rk3568",
     "enable_ramdisk": true,
     "build_selinux": true,
     "subsystems": [
   ########################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   ########################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },{
     "product_name": "rk3568",
     "product_company": "hihope",
     "product_device": "rk3568",
     "version": "2.0",
     "type": "standard",
     "product_build_path": "device/board/hihope/rk3568",
     "parts":{
       "ace:ace_engine_standard":{},
       "ace:napi":{},
   ################################################
       "third_tools:tools":{},
   ################################################
       "account:os_account_standard":{},
       "barrierfree:accessibility":{},
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的aio-stress。

## 三、编译命令

### 3.1基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/third_tools/ltp:aio
```

### 3.2基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/third_tools/ltp:aio
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：aio-stress的可执行文件放入（D:\test\aio-stress) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将aio-stress可执行文件发送到设备端

   ```
   hdc file send D:\test\aio-stress /data/test/
   ```

4. 在windows命令行上输入命令，进行板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./aio
   ```

## 四、aio-stress中的参数

| 参数     | 含义                                                         |
| -------- | ------------------------------------------------------------ |
| -a       | 以KB为单位对齐缓冲区的大小                                   |
| -b       | 一次提交io_submit的最大iocbs数量                             |
| -c       | 每个文件的IO上下文数                                         |
| -C       | 上下文之间的偏移量，默认为2MB                                |
| -s       | 测试文件的大小，默认为1024MB                                 |
| -r       | 用于每个io的记录大小(以KB为单位)，默认64KB                   |
| -d       | 每个文件挂起的aio请求数，默认为64                            |
| -i       | 在切换前每个文件发送的ios数量                                |
| -O       | 使用O_DIRECT(在2.4内核中不可用)                              |
| -S       | 写时使用O_SYNC                                               |
| -o       | 为列表添加一个操作:write=0, read=1 ，random write=2, random read=3 |
| -m shm   | 使用ipc共享内存作为IO缓冲区代替malloc                        |
| -m shmfs | 将一个文件映射到/dev/shm中用于IO缓冲区                       |
| -n       | 写阶段和读阶段之间无fsync                                    |
| -l       | 打印每个阶段后的io_submit延迟                                |
| -t       | 要运行的线程数                                               |
| -v       | 对写入字节进行校验                                           |
| -x       | 关闭线程阻碍协议                                             |
| -h       | 参数信息                                                     |

## 五、测试实例

### 5.1aio-stress在hi3516dv300测试示例：

#### ./aio [-s]  [-a]  [-b]  [-t]  [-c]  [file]的使用

[-s]	测试文件的大小，默认为1024MB

[-a]	以KB为单位对齐缓冲区的大小

[-b]	一次提交io_submit的最大iocbs数量

[-t]	要运行的线程数

[-c]	每个文件的IO上下文数

[file] 所测文件路径

**命令：**

```
./aio -s 128M -a 4KB -b 8 -t 4 -c 8 /data/tmp/aio-stress-file1 
```

**输出结果：**

```
file size 128MB, record size 64KB, depth 64, I/O per iteration 8
max io_submit 8, buffer alignment set to 0KB
threads 4 files 1 contexts 8 context offset 2MB verification off
Running multi thread version num_threads:4
write on /data/tmp/aio-stress-file1 (3.82 MB/s) 100.00 MB in 26.19s
write on /data/tmp/aio-stress-file1 (3.83 MB/s) 104.00 MB in 27.19s
write on /data/tmp/aio-stress-file1 (4.10 MB/s) 112.00 MB in 27.30s
write on /data/tmp/aio-stress-file1 (3.85 MB/s) 108.00 MB in 28.02s
write on /data/tmp/aio-stress-file1 (4.11 MB/s) 116.00 MB in 28.21s
write on /data/tmp/aio-stress-file1 (4.31 MB/s) 124.00 MB in 28.80s
write on /data/tmp/aio-stress-file1 (4.17 MB/s) 120.00 MB in 28.79s
write on /data/tmp/aio-stress-file1 (4.40 MB/s) 128.00 MB in 29.08s
thread 0 write totals (7.60 MB/s) 240.00 MB in 31.60s
thread 1 write totals (7.34 MB/s) 232.00 MB in 31.61s
thread 3 write totals (6.84 MB/s) 216.00 MB in 31.59s
thread 2 write totals (7.09 MB/s) 224.00 MB in 31.60s
write throughput (28.85 MB/s) 912.00 MB in 31.61s min transfer 224.00MB
read on /data/tmp/aio-stress-file1 (65.77 MB/s) 100.00 MB in 1.52s
read on /data/tmp/aio-stress-file1 (70.92 MB/s) 116.00 MB in 1.64s
thread 3 read totals (129.37 MB/s) 216.00 MB in 1.67s
read on /data/tmp/aio-stress-file1 (61.72 MB/s) 102.50 MB in 1.66s
read on /data/tmp/aio-stress-file1 (61.69 MB/s) 102.50 MB in 1.66s
read on /data/tmp/aio-stress-file1 (61.07 MB/s) 102.00 MB in 1.67s
read on /data/tmp/aio-stress-file1 (61.07 MB/s) 102.00 MB in 1.67s
read on /data/tmp/aio-stress-file1 (65.45 MB/s) 110.00 MB in 1.68s
read on /data/tmp/aio-stress-file1 (65.48 MB/s) 110.00 MB in 1.68s
thread 1 read totals (121.32 MB/s) 205.00 MB in 1.69s
thread 2 read totals (119.46 MB/s) 204.00 MB in 1.71s
thread 0 read totals (128.66 MB/s) 220.00 MB in 1.71s
read throughput (491.37 MB/s) 845.00 MB in 1.72s min transfer 220.00MB
random write on /data/tmp/aio-stress-file1 (1.23 MB/s) 100.00 MB in 81.18s
random write on /data/tmp/aio-stress-file1 (1.18 MB/s) 104.00 MB in 87.88s
random write on /data/tmp/aio-stress-file1 (1.29 MB/s) 116.00 MB in 89.91s
random write on /data/tmp/aio-stress-file1 (1.25 MB/s) 120.00 MB in 96.24s
random write on /data/tmp/aio-stress-file1 (1.16 MB/s) 112.00 MB in 96.68s
random write on /data/tmp/aio-stress-file1 (1.12 MB/s) 108.00 MB in 96.73s
random write on /data/tmp/aio-stress-file1 (1.27 MB/s) 128.00 MB in 100.40s
random write on /data/tmp/aio-stress-file1 (1.23 MB/s) 124.00 MB in 100.42s
thread 3 random write totals (2.07 MB/s) 216.00 MB in 104.34s
thread 0 random write totals (2.30 MB/s) 240.00 MB in 104.35s
thread 2 random write totals (2.15 MB/s) 224.00 MB in 104.36s
thread 1 random write totals (2.22 MB/s) 232.00 MB in 104.36s
random write throughput (8.74 MB/s) 912.00 MB in 104.36s min transfer 232.00MB
random read on /data/tmp/aio-stress-file1 (90.19 MB/s) 100.00 MB in 1.11s
random read on /data/tmp/aio-stress-file1 (95.68 MB/s) 116.00 MB in 1.21s
thread 3 random read totals (174.43 MB/s) 216.00 MB in 1.24s
random read on /data/tmp/aio-stress-file1 (58.38 MB/s) 73.00 MB in 1.25s
random read on /data/tmp/aio-stress-file1 (58.41 MB/s) 73.00 MB in 1.25s
thread 2 random read totals (115.91 MB/s) 146.00 MB in 1.26s
random read on /data/tmp/aio-stress-file1 (63.90 MB/s) 80.50 MB in 1.26s
random read on /data/tmp/aio-stress-file1 (64.25 MB/s) 81.00 MB in 1.26s
random read on /data/tmp/aio-stress-file1 (70.67 MB/s) 88.50 MB in 1.25s
random read on /data/tmp/aio-stress-file1 (70.69 MB/s) 88.50 MB in 1.25s
thread 1 random read totals (125.09 MB/s) 161.50 MB in 1.29s
thread 0 random read totals (137.18 MB/s) 177.00 MB in 1.29s
random read throughput (534.79 MB/s) 700.50 MB in 1.31s min transfer 177.00MB
```

### 5.2aio-stress在RK3568的测试示例：

#### ./aio [-s]  [-a]  [-b]  [-t]  [-c]  [file]的使用

[-s]	测试文件的大小，默认为1024MB

[-a]	以KB为单位对齐缓冲区的大小

[-b]	一次提交io_submit的最大iocbs数量

[-t]	要运行的线程数

[-c]	每个文件的IO上下文数

[file] 所测文件路径

**命令：**

```
./aio -s 128M -a 4KB -b 8 -t 4 -c 8 /data/tmp/aio-stress-file1
```

**输出结果：**

```
file size 128MB, record size 64KB, depth 64, I/O per iteration 8
max io_submit 8, buffer alignment set to 0KB
threads 4 files 1 contexts 8 context offset 2MB verification off
Running multi thread version num_threads:4
write on /data/tmp/aio-stress-file1 (97.06 MB/s) 108.00 MB in 1.11s
write on /data/tmp/aio-stress-file1 (100.69 MB/s) 124.00 MB in 1.23s
write on /data/tmp/aio-stress-file1 (82.57 MB/s) 104.00 MB in 1.26s
write on /data/tmp/aio-stress-file1 (86.45 MB/s) 112.00 MB in 1.30s
write on /data/tmp/aio-stress-file1 (90.34 MB/s) 120.00 MB in 1.33s
write on /data/tmp/aio-stress-file1 (93.47 MB/s) 128.00 MB in 1.37s
write on /data/tmp/aio-stress-file1 (77.76 MB/s) 100.00 MB in 1.29s
write on /data/tmp/aio-stress-file1 (85.52 MB/s) 116.00 MB in 1.36s
thread 1 write totals (125.28 MB/s) 232.00 MB in 1.85s
thread 3 write totals (116.63 MB/s) 216.00 MB in 1.85s
thread 0 write totals (129.59 MB/s) 240.00 MB in 1.85s
thread 2 write totals (120.95 MB/s) 224.00 MB in 1.85s
write throughput (492.37 MB/s) 912.00 MB in 1.85s min transfer 224.00MB
read on /data/tmp/aio-stress-file1 (207.18 MB/s) 100.00 MB in 0.48s
read on /data/tmp/aio-stress-file1 (221.02 MB/s) 116.00 MB in 0.52s
read on /data/tmp/aio-stress-file1 (195.29 MB/s) 104.00 MB in 0.53s
thread 3 read totals (405.34 MB/s) 216.00 MB in 0.53s
read on /data/tmp/aio-stress-file1 (152.65 MB/s) 81.00 MB in 0.53s
read on /data/tmp/aio-stress-file1 (152.61 MB/s) 81.50 MB in 0.53s
read on /data/tmp/aio-stress-file1 (196.09 MB/s) 104.50 MB in 0.53s
read on /data/tmp/aio-stress-file1 (190.78 MB/s) 101.50 MB in 0.53s
read on /data/tmp/aio-stress-file1 (190.84 MB/s) 102.00 MB in 0.53s
thread 0 read totals (303.92 MB/s) 162.50 MB in 0.53s
thread 2 read totals (389.92 MB/s) 208.50 MB in 0.53s
thread 1 read totals (380.41 MB/s) 203.50 MB in 0.53s
read throughput (1477.36 MB/s) 790.50 MB in 0.54s min transfer 203.50MB
random write on /data/tmp/aio-stress-file1 (95.54 MB/s) 100.00 MB in 1.05s
random write on /data/tmp/aio-stress-file1 (103.46 MB/s) 108.00 MB in 1.04s
random write on /data/tmp/aio-stress-file1 (105.52 MB/s) 116.00 MB in 1.10s
random write on /data/tmp/aio-stress-file1 (101.81 MB/s) 112.00 MB in 1.10s
random write on /data/tmp/aio-stress-file1 (92.54 MB/s) 104.00 MB in 1.12s
random write on /data/tmp/aio-stress-file1 (105.45 MB/s) 124.00 MB in 1.18s
random write on /data/tmp/aio-stress-file1 (107.84 MB/s) 128.00 MB in 1.19s
random write on /data/tmp/aio-stress-file1 (100.49 MB/s) 120.00 MB in 1.19s
thread 1 random write totals (98.31 MB/s) 232.00 MB in 2.36s
thread 0 random write totals (101.70 MB/s) 240.00 MB in 2.36s
thread 2 random write totals (94.92 MB/s) 224.00 MB in 2.36s
thread 3 random write totals (91.53 MB/s) 216.00 MB in 2.36s
random write throughput (386.42 MB/s) 912.00 MB in 2.36s min transfer 216.00MB
random read on /data/tmp/aio-stress-file1 (209.15 MB/s) 100.00 MB in 0.48s
random read on /data/tmp/aio-stress-file1 (223.54 MB/s) 116.00 MB in 0.52s
random read on /data/tmp/aio-stress-file1 (207.84 MB/s) 108.00 MB in 0.52s
thread 3 random read totals (409.19 MB/s) 216.00 MB in 0.53s
random read on /data/tmp/aio-stress-file1 (127.67 MB/s) 67.50 MB in 0.53s
random read on /data/tmp/aio-stress-file1 (211.28 MB/s) 111.00 MB in 0.53s
random read on /data/tmp/aio-stress-file1 (181.41 MB/s) 95.50 MB in 0.53s
random read on /data/tmp/aio-stress-file1 (127.82 MB/s) 67.50 MB in 0.53s
random read on /data/tmp/aio-stress-file1 (181.72 MB/s) 95.50 MB in 0.53s
thread 2 random read totals (255.06 MB/s) 135.00 MB in 0.53s
thread 0 random read totals (362.39 MB/s) 191.00 MB in 0.53s
thread 1 random read totals (415.47 MB/s) 219.00 MB in 0.53s
random read throughput (1436.92 MB/s) 761.00 MB in 0.53s min transfer 219.00MB
```

