# fio命令参数列表

## 一、详细命令参数（cmdhelp）

| 参数                          | 参数介绍                                                     |
| :---------------------------- | ------------------------------------------------------------ |
| name                          | 项目名称                                                     |
| wait_for                      | 开始之前要等待的项目的名称                                   |
| filename                      | 用于工作负载的文件                                           |
| lockfile                      | 对文件进行IO时锁定文件                                       |
| directory                     | 存放文件的目录                                               |
| filename_format               | 覆盖默认的$jobname.$jobnum.$filenum naming                   |
| unique_filename               | 对于网络客户端，用源IP前缀文件                               |
| opendir                       | 从这个目录下递归地添加文件                                   |
| rw                            | IO方式                                                       |
| bs                            | 块大小单位                                                   |
| ba                            | IO块偏移对齐                                                 |
| bsrange                       | 设置块大小范围(比bs更详细)                                   |
| bssplit                       | 设置特定混合的块大小                                         |
| bs_unaligned                  | 禁止调整IO缓冲区区域                                         |
| randrepeat                    | 使用可重复的随机IO模式                                       |
| randseed                      | 设置随机生成器种子值                                         |
| norandommap                   | 接受潜在的重复随机块                                         |
| ignore_error                  | 设置要忽略的特定错误列表                                     |
| rw_sequencer                  | IO偏移发生器修改器                                           |
| ioengine                      | 使用的IO引擎                                                 |
| iodepth                       | IO缓冲区的数量                                               |
| iodepth_batch                 | 一次性提交IO缓冲区的数量                                     |
| iodepth_batch_complete_min    | 一次检索IO缓冲区的最小数量                                   |
| iodepth_batch_complete_max    | 一次性检索IO缓冲区的最大数量                                 |
| iodepth_low                   | 低IO深度                                                     |
| serialize_overlap             | 等待正在碰撞的IOs完成                                        |
| io_submit_mode                | IO提交和完成的方式                                           |
| size                          | 设备或文件的总大小                                           |
| io_size                       | 需要执行的I/O总大小                                          |
| fill_device                   | 写入直到ENOSPC错误发生                                       |
| filesize                      | 单个文件的大小                                               |
| file_append                   | IO将从文件末尾开始                                           |
| offset                        | IO从这个偏移量开始                                           |
| offset_increment              | 从一个偏移量到下一个偏移量的增量                             |
| offset_align                  | 从偏移对齐开始IO                                             |
| number_ios                    | 在这个IOs数之后强制任务完成                                  |
| random_generator              | 要使用的随机数生成器类型                                     |
| random_distribution           | 随机偏移分布生成器                                           |
| percentage_random             | 应该是随机的seq/random混合的百分比                           |
| allrandrepeat                 | 所有内容都使用可重复的随机数                                 |
| nrfiles                       | 在这个数量的文件之间分割作业负载                             |
| file_service_type             | 如何选择下一步服务的文件                                     |
| openfiles                     | 同时打开的文件数                                             |
| fallocate                     | 布局文件时是否进行预分配                                     |
| fadvise_hint                  | 使用fadvise()在IO模式上建议内核                              |
| fsync                         | 对每个给定数量的块进行写操作                                 |
| fdatasync                     | 对每个给定数量的块发出fdatasync                              |
| write_barrier                 | 使第n次写变为barrier写                                       |
| sync_file_range               | 使用sync_file_range()                                        |
| direct                        | 使用O_DIRECT IO(否定缓冲)                                    |
| atomic                        | 在O_DIRECT中使用atomic IO(暗指O_DIRECT)                      |
| buffered                      | 使用缓冲IO(否定直接)                                         |
| overwrite                     | 写入时，设置是否覆盖当前数据                                 |
| loops                         | 运行作业的次数                                               |
| numjobs                       | 重复这个工作很多次                                           |
| startdelay                    | 只有在这个时间段过后才开始作业                               |
| runtime                       | 过了这段时间后停止工作负载                                   |
| time_based                    | 持续运行，直到满足runtime/timeout                            |
| verify_only                   | 验证以前写入的数据是否仍然有效                               |
| ramp_time                     | 测量性能前的上升时间                                         |
| clocksource                   | 使用哪种类型的定时源                                         |
| mem                           | IO缓冲区的后备类型                                           |
| verify                        | 校验写入的数据                                               |
| do_verify                     | 写后运行验证阶段                                             |
| verify_interval               | 每N个字节存储校验缓冲区头                                    |
| verify_offset                 | 将验证头位置偏移N个字节                                      |
| verify_pattern                | 填充IO缓冲区的模式                                           |
| verify_fatal                  | 在单个验证失败时退出，不要继续                               |
| verify_dump                   | 失败时转储好块和坏块的内容                                   |
| verify_async                  | 要使用的异步验证器线程数                                     |
| verify_backlog                | 在写完这些块之后进行验证                                     |
| verify_backlog_batch          | 验证IO块的数量                                               |
| experimental_verify           | 开启实验验证                                                 |
| verify_state_load             | 加载验证终止状态                                             |
| verify_state_save             | 在终止时保存验证状态                                         |
| trim_percentage               | 要修剪的校验块的数量(即丢弃)                                 |
| trim_verify_zero              | 验证修剪(即丢弃)的块以零的形式返回                           |
| trim_backlog                  | 在写完这些块之后进行修剪                                     |
| trim_backlog_batch            | 修剪IO块的数量                                               |
| write_iolog                   | 存储IO模式到文件                                             |
| read_ilog                     | 回放文件的IO模式                                             |
| read_iolog_chunked            | 用块解析IO模式                                               |
| replay_no_stall               | 在没有停顿的情况下尽可能快地播放IO模式文件                   |
| replay_redirect               | 重放所有I/O到这个设备上，不管跟踪设备                        |
| replay_scale                  | 将偏移量对齐到这个块大小                                     |
| replay_align                  | 按此系数向下缩放                                             |
| replay_time_scale             | 回放事件的缩放时间                                           |
| replay_skip                   | 跳过某些IO类型(读，写，修剪，刷新)                           |
| merge_blktrace_file           | 合并blktrace输出文件名                                       |
| merge_blktrace_scalars        | 缩放每个跟踪的百分比                                         |
| merge_blktrace_iter           | 每个跟踪运行的迭代次数                                       |
| exec_prerun                   | 在运行作业之前执行这个文件                                   |
| exec_poststrun                | 在运行job之后执行这个文件                                    |
| ioscheduler                   | 在后备设备上使用这个IO调度程序                               |
| zonmode                       | zonesize、zonerange和zoneskip参数的模式                      |
| zonesize                      | 每个区域要读取的数据量                                       |
| zoneccapacity                 | 每个分区的容量                                               |
| zonerange                     | 给出IO区域的大小                                             |
| zoneskip                      | IO分区之间的空间                                             |
| read_beyond_wp                | 允许读取超出区域写指针                                       |
| max_open_zones                | 使用zonemode=zbd限制同时打开的顺序写zone的数量               |
| job_max_open_zones            | 限制一个线程/进程使用zonemode=zbd同时打开的顺序写区域的数量  |
| ignore_zone_limits            | 忽略设备报告的区域资源限制(max open/active zone)             |
| zone_reset_threshold          | 分区块设备复位阈值                                           |
| zone_reset_frequency          | 被分区的块设备分区重置频率，以HZ为单位                       |
| lockmem                       | 锁定每个worker的内存数量                                     |
| rwmixread                     | 读取的混合工作负载百分比                                     |
| rwmixwrite                    | 写的混合工作负载的百分比                                     |
| nice                          | 设置任务CPU nice值                                           |
| prio                          | 设置任务IO优先级值                                           |
| prioclass                     | 设置作业IO优先级类                                           |
| thinktime                     | IO缓冲区之间的空闲时间(usec)                                 |
| thinktime_spin                | 通过旋转这个量开始思考时间(usec)                             |
| thinktime_blocks              | 在'thinktime'之间的IO缓冲时间                                |
| thinktime_blocks_type         | thinktime_blocks如何生效                                     |
| thinktime_iotime              | IO时间与thinktime之间的间隔                                  |
| rate                          | 设置带宽速率                                                 |
| rate_min                      | 作业必须满足该速率，否则将被关闭                             |
| rate_process                  | 哪个进程控制如何管理额定IO                                   |
| rate_cycle                    | 速率限制的窗口平均值(msec)                                   |
| rate_ignore_thinktime         | 额定IO忽略thinktime设置                                      |
| rate_iops                     | 每秒IO次数限制                                               |
| rate_iops_min                 | 作业必须满足这个速率，否则将被关闭                           |
| max_latency                   | 最大允许IO时延(usec)                                         |
| latency_target                | Ramp到最大队列深度支持此延迟                                 |
| latency_window                | 维持latency_target的时间                                     |
| latency_percentile            | IOs的百分比必须低于latency_target                            |
| latency_run                   | 不断调整队列深度以匹配latency_target                         |
| invalidate                    | 在运行作业之前，使缓冲区/页缓存失效                          |
| sync                          | 使用同步写IO                                                 |
| write_hint                    | 设置预期的写生命周期                                         |
| create_serialize              | 序列化作业文件的创建                                         |
| create_fsync                  | 创建后的fsync文件                                            |
| create_on_open                | 在IO打开文件时创建文件                                       |
| create_only                   | 只执行文件创建阶段                                           |
| allow_file_create             | 允许fio创建不存在的文件                                      |
| allow_mounted_write           | 允许写入挂载的分区                                           |
| pre_read                      | 开始正式测试前预读文件                                       |
| cpumask                       | CPU亲和性掩码                                                |
| cpus_allowed                  | 允许使用的cpu数量                                            |
| cpus_allowed_policy           | cpus_allowed的分发策略                                       |
| numa_cpu_nodes                | NUMA CPU节点绑定                                             |
| numa_mem_policy               | 设置NUMA内存策略                                             |
| end_fsync                     | 在作业结束时包含fsync                                        |
| Fsync_on_close                | 关闭fsync文件                                                |
| unlink                        | 作业完成后，取消已创建文件的链接                             |
| unlink_each_loop              | 在作业的每次循环完成后，取消已创建文件的链接                 |
| exitall                       | 当一个作业退出时终止所有作业                                 |
| exit_what                     | exitall的细粒度控制                                          |
| exitall_on_error              | 当有一个错误退出时终止所有作业                               |
| stonewall                     | 在这份工作和之前的工作之间设置一道坚硬的障碍                 |
| new_group                     | 标记新组的开始(用于报告)                                     |
| thread                        | 使用线程而不是进程                                           |
| per_job_logs                  | 生成的日志文件中是否包含作业号                               |
| write_bw_log                  | 运行时带宽写日志                                             |
| bwavgtime                     | 计算带宽的时间窗口(msec)                                     |
| write_lat_log                 | 运行时延写日志                                               |
| write_iops_log                | 运行时IOPS写入日志                                           |
| iopsavgtime                   | 计算IOPS的时间窗口(msec)                                     |
| log_entries                   | 作业IO日志的初始条目数                                       |
| log_avg_msec                  | 一段时间内bw/iops/lat的平均日志                              |
| log_hist_msec                 | 以这个时间值的频率转储完成延迟直方图                         |
| log_hist_coarse               | 整数，范围[0,6]。粗糙度越高，每个样本输出的直方图箱就越少。这些容器的数量分别为[1216,608,304,152,76,38,19]。 |
| write_hist_log                | 运行时写时延直方图日志                                       |
| log_max_value                 | 窗口中的日志最大样本，而不是平均值                           |
| log_offset                    | 包含每个日志条目的IO偏移量                                   |
| log_prio                      | 包含每个日志条目的IO优先级值                                 |
| log_compression               | 记录此大小的压缩块                                           |
| log_compression_cpus          | 限制日志压缩到这些cpu                                        |
| log_store_compressed          | 以压缩格式存储日志                                           |
| log_unix_epoch                | 在日志文件中使用Unix时间                                     |
| log_alternate_epoch           | 在日志文件中使用备用epoch时间。使用与clock_gettime使用指定delog_alternate_epoch_clock_id相同的epoch |
| log_alternate_epoch_clock_id  | 如果log_alternate_epoch或log_unix_epoch为true，该选项指定clock_gettime中应该使用epoch的clock_id。如果两者都不为真，则此选项无效。默认值为0或CLOCK_REALTIME |
| block_error_percentiles       | 记录修剪块错误并制作直方图                                   |
| group_reporting               | 按组进行报告                                                 |
| stats                         | 启用统计信息的收集                                           |
| zero_buffers                  | 将IO缓冲区初始化为所有0                                      |
| refill_buffers                | 在每次IO提交时重新填充IO缓冲区                               |
| scramble_buffers              | 在每次IO提交时略微混乱缓冲区                                 |
| buffer_pattern                | 填充IO缓冲区的模式                                           |
| buffer_compress_percentage    | 缓冲区的可压缩程度(大约)                                     |
| buffer_compress_chunk         | 缓冲区中可压缩区域的大小                                     |
| dedupe_percentage             | 可扣除的缓冲区的百分比                                       |
| dedupe_mode                   | 重复数据删除缓冲区生成模式                                   |
| dedupe_working_set_percentage | 重复数据删除工作集大小，以生成重复数据删除模式的文件或设备大小的百分比表示 |
| clat_percentiles              | 启用完成延迟百分比报告                                       |
| lat_percentiles               | 启用IO延迟百分比报告                                         |
| slat_percentiles              | 启用提交延迟百分比的报告                                     |
| percentile_list               | 指定一个百分比的自定义列表，用于报告完成延迟和块错误         |
| significant_figures           | 设置为正常输出格式的有效数字                                 |
| disk_util                     | 日志硬盘利用率统计信息                                       |
| gtod_reduce                   | 大大减少gettimeofday()调用的次数                             |
| disable_lat                   | 禁用延迟数                                                   |
| disable_clat                  | 禁用完成延迟数                                               |
| disable_slat                  | 禁用提交延迟数                                               |
| disable_bw_measurement        | 禁用带宽日志记录                                             |
| gtod_cpu                      | 在这个CPU上设置专用的gettimeofday()线程                      |
| unified_rw_reporting          | 跨数据方向统一报告                                           |
| continue_on_error             | 在IO期间继续非致命错误                                       |
| error_dump                    | 转储每个错误的信息                                           |
| profile                       | 选择特定的内置性能测试                                       |
| cgroup                        | 将作业添加到该名称的cgroup中                                 |
| cgroup_nodelete               | 任务完成后不删除cgroup                                       |
| cgroup_weight                 | 使用给定的cgroup权重                                         |
| uid                           | 使用此用户ID运行job                                          |
| gid                           | 使用此组ID运行job                                            |
| kb_base                       | 数据量的单位前缀解释(IEC和SI)                                |
| unit_base                     | 结果汇总数据的位倍数(字节8，位1)                             |
| hupage -size                  | 使用hupage时，指定每个页面的大小                             |
| flow_id                       | 要使用的流索引ID                                             |
| flow                          | 该作业流量控制的重量                                         |
| flow_sleep                    | 在被流量控制机制抑制后需要休眠多少微秒                       |
| steadystate                   | 定义判断作业何时达到稳态的标准和极限                         |
| steadystate_duration          | 在达到指定的稳态持续时间后停止工作负载                       |
| steadystate_ramp_time         | 启动稳态作业终止测试的数据收集前的延迟                       |

## 二、特定的引擎ioengine的命令参数

有一些参数仅在使用特定ioengine时才有效。这些与普通参数的使用方式相同，但需要注意的是，在命令行上使用时，它们必须在选择定义它们的ioengine之后出现。

### 1、引擎libaio的特定参数

| 引擎参数                             | 参数介绍                                                     |
| ------------------------------------ | ------------------------------------------------------------ |
| **cmdprio_percentage** = *int[,int]* | 设置将以最高优先级发出的 I/O 百分比。默认值：0。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。此选项不能与 `prio` 或 `prioclass` 选项一起使用。要使此选项生效，必须支持并启用 NCQ 优先级，并且必须使用 `direct=1' 选项。fio 也必须以 root 用户身份运行。 |
| **cmdprio_class** = *int[,int]*      | 设置 I/O 优先级类以用于在设置**cmdprio_percentage**或**cmdprio_bssplit**时必须以优先级发出的 I/O 。如果在设置**cmdprio_percentage**或**cmdprio_bssplit**时未指定，则默认为最高优先级。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。 |
| **cmdprio** = *int[,int]*            | 设置 I/O 优先级值以用于在设置**cmdprio_percentage**或**cmdprio_bssplit**时必须以优先级发出的 I/O 。如果在设置**cmdprio_percentage**或**cmdprio_bssplit**时未指定，则默认为 0。Linux 将我们限制为 0 到 7 之间的正值，其中 0 为最高值。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。 |
| **cmdprio_bssplit** = *str[,str]*    | 为了更好地控制 I/O 优先级，此选项允许指定必须根据 IO 的块大小设置优先级的 IO 百分比。此选项仅在与选项**bssplit**一起使用时才有用，即多个不同的块大小用于读取和写入。此选项的格式与**bssplit**选项的格式相同，但会忽略修剪 IO 的值。此选项与**cmdprio_percentage**选项互斥。 |
| **userspace_reap**                   | 通常，在使用 libaio 引擎的情况下，fio 将使用**io_getevents** (3) 系统调用来获取新返回的事件。打开此标志后，将直接从用户空间读取 AIO 环以获取事件。仅当轮询至少 0 个事件时才启用收割模式（例如，当 `iodepth_batch_complete=0' 时）。 |
| **nowait**                           | 默认情况下，如果一个请求不能立即执行（例如资源匮乏、等待锁），它会被排队并且初始化进程将被阻塞，直到所需的资源变得空闲。此选项设置 RWF_NOWAIT 标志（受 4.14 Linux 内核支持）并且调用将立即返回 EAGAIN 或部分结果而不是等待。 |

### 2、引擎io_uring的特定参数

| 引擎参数                             | 参数介绍                                                     |
| ------------------------------------ | ------------------------------------------------------------ |
| **cmdprio_percentage** = *int[,int]* | 设置将以最高优先级发出的 I/O 百分比。默认值：0。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。此选项不能与 `prio` 或 `prioclass` 选项一起使用。要使此选项生效，必须支持并启用 NCQ 优先级，并且必须使用 `direct=1' 选项。fio 也必须以 root 用户身份运行。 |
| **cmdprio_class** = *int[,int]*      | 设置 I/O 优先级类以用于在设置**cmdprio_percentage**或**cmdprio_bssplit**时必须以优先级发出的 I/O 。如果在设置**cmdprio_percentage**或**cmdprio_bssplit**时未指定，则默认为最高优先级。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。 |
| **cmdprio** = *int[,int]*            | 设置 I/O 优先级值以用于在设置**cmdprio_percentage**或**cmdprio_bssplit**时必须以优先级发出的 I/O 。如果在设置**cmdprio_percentage**或**cmdprio_bssplit**时未指定，则默认为 0。Linux 将我们限制为 0 到 7 之间的正值，其中 0 为最高值。单个值适用于读取和写入。可以为读取和写入指定逗号分隔的值。 |
| **cmdprio_bssplit** = *str[,str]*    | 为了更好地控制 I/O 优先级，此选项允许指定必须根据 IO 的块大小设置优先级的 IO 百分比。此选项仅在与选项**bssplit**一起使用时才有用，即多个不同的块大小用于读取和写入。此选项的格式与**bssplit**选项的格式相同，但会忽略修剪 IO 的值。此选项与**cmdprio_percentage**选项互斥。 |
| **fixedbufs**                        | 如果要求 fio 进行直接 IO，那么 Linux 会为每个 IO 调用映射页面，并在 IO 完成时释放它们。如果设置了此选项，则在启动 IO 之前预先映射页面。这消除了为每个 IO 映射和释放的需要。这样效率更高，并且也减少了 IO 延迟。 |
| **hipri**                            | 如果设置了此选项，fio 将尝试使用轮询 IO 完成。正常的 IO 完成会产生中断来表示 IO 的完成，而轮询完成则不会。因此，它们需要应用程序主动获取。好处是高 IOPS 场景的 IO 更高效，低队列深度 IO 的延迟更低。 |
| **registerfiles**                    | 使用此选项，fio 注册与内核一起使用的文件集。这避免了在内核中管理文件计数的开销，使提交和完成部分更加轻量。以下 sqthread_poll 选项是必需的。 |
| **sqthread_poll**                    | 通常，fio 会通过发出系统调用来提交 IO，以通知内核 SQ 环中的可用项。如果设置了这个选项，提交 IO 的动作将由内核中的轮询线程完成。这释放了 fio 的周期，代价是在系统中使用更多 CPU。 |
| **sqthread_poll_cpu**                | 当设置了 `sqthread_poll` 时，该选项提供了一种方法来定义哪个 CPU 应该用于轮询线程。 |
| **nowait**                           | 默认情况下，如果一个请求不能立即执行（例如资源匮乏、等待锁），它会被排队并且初始化进程将被阻塞，直到所需的资源变得空闲。此选项设置 RWF_NOWAIT 标志（受 4.14 Linux 内核支持）并且调用将立即返回 EAGAIN 或部分结果而不是等待。 |

### 3、引擎**pvsync2**的特定参数

| 引擎参数             | 参数介绍                                                     |
| -------------------- | ------------------------------------------------------------ |
| **hipri**            | 在 I/O 上设置 RWF_HIPRI，向内核表明它的优先级高于正常。      |
| **hipri_percentage** | 当设置 hipri 时，这决定了 pvsync2 I/O 为高优先级的概率。默认值为 100%。 |
| **nowait**           | 默认情况下，如果一个请求不能立即执行（例如资源匮乏、等待锁），它会被排队并且初始化进程将被阻塞，直到所需的资源变得空闲。此选项设置 RWF_NOWAIT 标志（受 4.14 Linux 内核支持）并且调用将立即返回 EAGAIN 或部分结果而不是等待。 |

### 4、引擎cpuio的特定参数

| 引擎参数                     | 参数介绍                                                     |
| ---------------------------- | ------------------------------------------------------------ |
| **cpuload** = *int*          | 尝试使用指定百分比的 CPU 周期。这是使用 cpuio I/O 引擎时的强制选项。 |
| **cpuchunks** = *int*        | 将负载拆分为给定时间的周期。以微秒为单位。                   |
| **exit_on_io_done** = *bool* | 检测 I/O 线程何时完成，然后退出。                            |

### 5、引擎libhdfs的特定参数

| 引擎参数           | 参数介绍                                       |
| ------------------ | ---------------------------------------------- |
| **namenode**=*str* | 要联系的 HDFS 集群名称节点的主机名或 IP 地址。 |
| **port**=*int*     | HFDS集群namenode的监听端口。                   |
| **hdfsdirectory**  | libhdfs 将在此 HDFS 目录中创建块。             |
| **chunk_size**     | 用于每个文件的块的大小。                       |

### 6、引擎**netsplice**的特定参数

| 引擎参数                                 | 参数介绍                                                     |
| ---------------------------------------- | ------------------------------------------------------------ |
| **port** = *int*                         | 要绑定或连接到的 TCP 或 UDP 端口。如果这与**numjobs**一起使用以生成相同作业类型的多个实例，那么这将是起始端口号，因为 fio 将使用一系列端口。 |
| **hostname**=*str*                       | 用于基于 TCP、UDP 或 RDMA-CM 的 I/O 的主机名或 IP 地址。如果作业是 TCP 侦听器或 UDP 读取器，则不使用主机名并且必须省略，除非它是有效的 UDP 多播地址。 |
| **interface** = *str*                    | 用于发送或接收 UDP 多播的网络接口的 IP 地址。                |
| **ttl** = *int*                          | 传出 UDP 多播数据包的生存时间值。默认值：1。                 |
| **nodelay** = *bool*                     | 在 TCP 连接上设置 TCP_NODELAY。                              |
| **protocol** = *str* , **proto** = *str* | 要使用的网络协议。可接受的值为：**tcp**：传输控制协议。<br />                                                              **tcpv6**：传输控制协议V6。<br />                                                              **UDP**:用户数据报协议。<br />                                                              **udpv6**:用户数据报协议 V6。<br />                                                              **Unix**:UNIX 域套接字。<br />当协议是 TCP 或 UDP 时，还必须给出端口，如果作业是 TCP 侦听器或 UDP 读取器，则还必须给出主机名。对于 unix 套接字，应该使用普通文件名选项并且端口无效。 |
| **listen**                               | 对于 TCP 网络连接，告诉 fio 侦听传入连接而不是启动传出连接。如果使用此选项，则必须省略主机名。 |
| **pingpong**                             | 通常网络写入器只会继续写入数据，而网络读取器只会消耗包。如果设置了 `pingpong=1'，写入器会将其正常的有效负载发送给读取器，然后等待读取器发送回相同的有效负载。这允许 fio 测量网络延迟。然后提交和完成延迟测量本地发送或接收所花费的时间，完成延迟测量另一端接收和发送回所花费的时间。对于 UDP 多播流量 `pingpong=1' 应该只在多个阅读器监听同一个地址时为单个阅读器设置。 |
| **window_size**=*int*                    | 为连接设置所需的套接字缓冲区大小。                           |
| **mss**=*int*                            | 设置 TCP 最大段大小 (TCP_MAXSEG)。                           |

### 7、引擎net的特定参数

| 引擎参数                                 | 参数介绍                                                     |
| ---------------------------------------- | ------------------------------------------------------------ |
| **port** = *int*                         | 要绑定或连接到的 TCP 或 UDP 端口。如果这与**numjobs**一起使用以生成相同作业类型的多个实例，那么这将是起始端口号，因为 fio 将使用一系列端口。 |
| **hostname**=*str*                       | 用于基于 TCP、UDP 或 RDMA-CM 的 I/O 的主机名或 IP 地址。如果作业是 TCP 侦听器或 UDP 读取器，则不使用主机名并且必须省略，除非它是有效的 UDP 多播地址。 |
| **interface** = *str*                    | 用于发送或接收 UDP 多播的网络接口的 IP 地址。                |
| **ttl** = *int*                          | 传出 UDP 多播数据包的生存时间值。默认值：1。                 |
| **nodelay** = *bool*                     | 在 TCP 连接上设置 TCP_NODELAY。                              |
| **protocol** = *str* , **proto** = *str* | 要使用的网络协议。可接受的值为：**tcp**：传输控制协议。<br />                                                              **tcpv6**：传输控制协议V6。<br />                                                              **UDP**:用户数据报协议。<br />                                                              **udpv6**:用户数据报协议 V6。<br />                                                              **Unix**:UNIX 域套接字。<br />当协议是 TCP 或 UDP 时，还必须给出端口，如果作业是 TCP 侦听器或 UDP 读取器，则还必须给出主机名。对于 unix 套接字，应该使用普通文件名选项并且端口无效。 |
| **listen**                               | 对于 TCP 网络连接，告诉 fio 侦听传入连接而不是启动传出连接。如果使用此选项，则必须省略主机名。 |
| **pingpong**                             | 通常网络写入器只会继续写入数据，而网络读取器只会消耗包。如果设置了 `pingpong=1'，写入器会将其正常的有效负载发送给读取器，然后等待读取器发送回相同的有效负载。这允许 fio 测量网络延迟。然后提交和完成延迟测量本地发送或接收所花费的时间，完成延迟测量另一端接收和发送回所花费的时间。对于 UDP 多播流量 `pingpong=1' 应该只在多个阅读器监听同一个地址时为单个阅读器设置。 |
| **window_size**=*int*                    | 为连接设置所需的套接字缓冲区大小。                           |
| **mss**=*int*                            | 设置 TCP 最大段大小 (TCP_MAXSEG)。                           |

### 8、引擎**rdma**的特定参数

| 引擎参数           | 参数介绍                                                     |
| ------------------ | ------------------------------------------------------------ |
| **port** = *int*   | 用于 RDMA-CM 通信的端口。这在客户端和服务器端应该是相同的值。 |
| **verb**=*str*     | 在 RDMA ioengine 连接的这一端使用的 RDMA 动词。有效值为 write、read、send 和 recv。这些对应于等效的 RDMA 动词（例如 write = rdma_write 等）。请注意，这只需要在连接的客户端指定。请参阅示例文件夹。 |
| **bindname**=*str* | 用于将本地 RDMA-CM 连接绑定到本地 RDMA 设备的名称。这可以是主机名或 IPv4 或 IPv6 地址。在服务器端，这将被传递到 rdma_bind_addr() 函数中，而在客户端站点上，它将在 rdma_resolve_add() 函数中使用。当客户端和服务器之间或在某些环回配置中存在多个路径时，这可能很有用。 |

### 9、引擎e4defrag的特定参数

| 引擎参数            | 参数介绍                                                     |
| ------------------- | ------------------------------------------------------------ |
| **donorname**=*str* | 文件将用作块捐赠者（文件之间的交换范围）。                   |
| **inplace**=*int*   | 配置施主文件块分配策略：<br />                  0           默认。在初始化时预分配捐助者的文件。<br />                  1           在碎片整理事件中立即分配空间，并在事件发生后立即释放。 |

### 10、引擎rbd的特定参数

| 引擎参数                | 参数介绍                                                     |
| ----------------------- | ------------------------------------------------------------ |
| **clustername** = *str* | 指定 Ceph 集群的名称。                                       |
| **rbdname** = *str*     | 指定 RBD 的名称。                                            |
| **pool** = *str*        | 指定包含 RBD 或 RADOS 数据的 Ceph 池的名称。                 |
| **clientname** = *str*  | 指定用于访问 Ceph 集群的用户名（不带“client.”前缀）。如果指定了集群名称，则**客户**端名称应为完整的 *type.id* 字符串**。**如果没有类型。给定前缀，fio 将添加“客户端”。默认。 |
| **busy_poll** = *bool*  | 轮询存储而不是等待完成。通常这会以更高（高达 100%）的 CPU 利用率为代价提供更好的吞吐量。 |

### 11、引擎rados的特定参数

| 引擎参数                   | 参数介绍                                                     |
| -------------------------- | ------------------------------------------------------------ |
| **clustername** = *str*    | 指定 Ceph 集群的名称。                                       |
| **pool** = *str*           | 指定包含 RBD 或 RADOS 数据的 Ceph 池的名称。                 |
| **clientname** = *str*     | 指定用于访问 Ceph 集群的用户名（不带“client.”前缀）。如果指定了集群名称，则**客户**端名称应为完整的 *type.id* 字符串**。**如果没有类型。给定前缀，fio 将添加“客户端”。默认。 |
| **busy_poll** = *bool*     | 轮询存储而不是等待完成。通常这会以更高（高达 100%）的 CPU 利用率为代价提供更好的吞吐量。 |
| **touch_objects** = *bool* | 在初始化期间，触摸（如果不存在则创建）所有对象（文件）。触摸所有对象会影响 ceph 缓存，并可能影响测试结果。默认启用。 |

### 12、引擎http的特定参数

| 引擎参数                          | 参数介绍                                                     |
| --------------------------------- | ------------------------------------------------------------ |
| **http_host** = *str*             | 要连接的主机名。对于 S3，这可能是存储桶名称。默认是**本地主机** |
| **http_user** = *str*             | HTTP 身份验证的用户名。                                      |
| **http_pass** = *str*             | HTTP 身份验证的密码。                                        |
| **https** =*str*                  | 是否使用 HTTPS 而不是普通的 HTTP。on 启用 HTTPS；insecure 将启用 HTTPS，但禁用 SSL 对等验证（谨慎使用！）。默认为**关闭**。 |
| **http_mode** = *str*             | 使用哪种 HTTP 访问模式：webdav、swift 或 s3。默认为**webdav**。 |
| **http_s3_region** = *str*        | 要包含在请求中的 S3 区域/区域。默认为**us-east-1**。         |
| **http_s3_key** = *str*           | S3 密钥。                                                    |
| **http_s3_keyid** = *str*         | S3 密钥/访问 ID。                                            |
| **http_swift_auth_token** = *str* | Swift 身份验证令牌。请参阅示例配置文件以了解如何检索它。     |
| **http_verbose** = *int*          | 启用来自 libcurl 的详细请求。对调试很有用。1 打开来自 libcurl 的详细日志记录，2 还启用 HTTP IO 跟踪。默认为**0** |

### 13、引擎mtd的特定参数

| 引擎参数              | 参数介绍                 |
| --------------------- | ------------------------ |
| **skip_bad** = *bool* | 跳过针对已知坏块的操作。 |

### 14、引擎rdma的特定参数

| 引擎参数           | 参数介绍                                                     |
| ------------------ | ------------------------------------------------------------ |
| **verb**=*str*     | 在 RDMA ioengine 连接的这一端使用的 RDMA 动词。有效值为 write、read、send 和 recv。这些对应于等效的 RDMA 动词（例如 write = rdma_write 等）。请注意，这只需要在连接的客户端指定。请参阅示例文件夹。 |
| **bindname**=*str* | 用于将本地 RDMA-CM 连接绑定到本地 RDMA 设备的名称。这可以是主机名或 IPv4 或 IPv6 地址。在服务器端，这将被传递到 rdma_bind_addr() 函数中，而在客户端站点上，它将在 rdma_resolve_add() 函数中使用。当客户端和服务器之间或在某些环回配置中存在多个路径时，这可能很有用。 |

### 15、引擎filestat的特定参数

| 引擎参数              | 参数介绍                                                     |
| --------------------- | ------------------------------------------------------------ |
| **stat_type** = *str* | 指定 stat 系统调用类型以测量查找/getattr 性能。默认为**stat(2)**的 stat 。 |

### 16、引擎sg的特定参数

| 引擎参数                | 参数介绍                                                     |
| ----------------------- | ------------------------------------------------------------ |
| **hipri**               | 如果设置了此选项，fio 将尝试使用轮询 IO 完成。这将具有与 (io_uring)hipri 类似的效果。只有 SCSI READ 和 WRITE 命令会设置 SGV4_FLAG_HIPRI（不是 UNMAP (trim) 也不是 VERIFY）。不支持 hipri 的旧版本的 Linux sg 驱动程序将简单地忽略此标志并执行正常 IO。“拥有”该设备的 Linux SCSI 低级驱动程序 (LLD) 还需要支持 hipri（也称为 iopoll 和 mq_poll）。MegaRAID 驱动程序是 SCSI LLD 的一个示例。默认值：clear (0) 执行正常（基于中断的）IO。 |
| **readfua**=*bool*      | 将 readfua 选项设置为 1 时，读取操作包括强制单元访问 (fua) 标志。默认值：0。 |
| **writefua**=*bool*     | 将 writefua 选项设置为 1，写入操作包括强制单元访问 (fua) 标志。默认值：0。 |
| **sg_write_mode**=*str* | 指定要发出的写入命令的类型。此选项可以采用三个值：<br />           **wirte（默认）**：写操作码照常发出<br />           **verify**：发出 WRITE AND VERIFY 命令。BYTCHK 位设置为 0。这指示设备执行不进行数据比较的介质验证。此选择将忽略 writefua 选项。<br />           **same**：发出 WRITE SAME 命令。这会将单个块传输到设备，并将同一数据块写入从指定偏移量开始的连续 LBA 序列。fio 的块大小参数指定每个命令写入的数据量。但是，实际传输到设备的数据量等于设备的块（扇区）大小。对于具有 512 字节扇区的设备，blocksize=8k 将使用每个命令写入 16 个扇区。fio 仍然会为每个命令生成 8k 的数据，但只会使用前 512 个字节并将其传输到设备。此选择将忽略 writefua 选项。 |

### 17、引擎**libcufile**的特定参数

| 引擎参数                | 参数介绍                                                     |
| ----------------------- | ------------------------------------------------------------ |
| **gpu_dev_ids**=**str** | 指定要与 CUDA 一起使用的 GPU ID。这是一个以冒号分隔的 int 列表。GPU 被分配给工人循环。默认值为 0。 |
| **cuda_io**=**str**     | 指定要与 CUDA 一起使用的 I/O 类型。此选项采用以下值：<br />**cufile（默认）**：使用 libcufile 和 nvidia-fs。此选项直接在 GPUDirect 存储文件系统和 GPU 缓冲区之间执行 I/O，避免使用反弹缓冲区。如果设置了**verify**，则 cudaMemcpy 用于在 RAM 和 GPU 之间复制验证数据。验证数据在写入之前从 RAM 复制到 GPU，在读取之后从 GPU 复制到 RAM。**直接**必须为 1。<br />**posix**：使用 POSIX 通过 RAM 缓冲区执行 I/O，并使用 cudaMemcpy 在 RAM 和 GPU 之间传输数据。数据在写入之前从 GPU 复制到 RAM，在读取之后从 RAM 复制到 GPU。**verify**不影响 cudaMemcpy 的使用。 |

### 18、引擎dfs的特定参数

| 引擎参数         | 参数介绍                                                     |
| ---------------- | ------------------------------------------------------------ |
| **pool**         | 指定要连接的 DAOS 池的标签或 UUID。                          |
| **cont**         | 指定要打开的 DAOS 容器的标签或 UUID。                        |
| **chunk_size**   | 为 dfs 文件指定不同的块大小（以字节为单位）。默认使用 DAOS 容器的块大小。 |
| **object_class** | 为 dfs 文件指定一个不同的对象类。默认使用 DAOS 容器的对象类。 |

### 19、引擎nfs的特定参数

| 引擎参数    | 参数介绍                                                     |
| ----------- | ------------------------------------------------------------ |
| **nfs_url** | ibnfs 格式的 URL，例如 nfs://<server\|ipv4\|ipv6>/path[?arg=val[&arg=val]*] |

### 20、引擎exec的特定参数

| 引擎参数                  | 参数介绍                                                     |
| ------------------------- | ------------------------------------------------------------ |
| **program**=**str**       | 指定要执行的程序。请注意，当作业达到时间限制时，程序将收到一个 SIGTERM。工作结束后会发送 SIGKILL。两个信号之间的延迟由**Grace_time**选项定义。 |
| **arguments**=**str**     | 指定要传递给程序的参数。可以扩展一些特殊变量以将 fio 的工作详细信息传递给程序：<br />**%r**：替换为作业的持续时间（以秒为单位）<br />**%n**：替换为工作名称 |
| **grace_time**=**int**    | 定义 SIGTERM 和 SIGKILL 信号之间的时间。默认值为 1 秒。      |
| **std_redirect**=**bool** | 如果设置，stdout 和 stderr 流将重定向到以作业名称命名的文件。默认为真。 |

## 三、线程、进程和作业同步的参数介绍

| 使用参数                             | 参数介绍                                                     |
| ------------------------------------ | ------------------------------------------------------------ |
| **replay_skip** = *str*              | 有时在重播跟踪中跳过某些 IO 类型很有用。例如，这可能是消除跟踪中的写入。或者不重播修剪/丢弃，如果您正在重定向到不支持它们的设备。此选项采用逗号分隔的读取、写入、修剪、同步列表。 |
| **thread**                           | Fio 默认使用 fork 创建作业，但是如果给出此选项，fio 将使用 POSIX 线程的函数pthread_create(3)创建作业来代替创建线程。 |
| **wait_for**=*str*                   | 如果设置，则在指定等待作业的所有工作人员完成之前，不会启动当前作业。wait_for基于作业名称进行操作，因此有一些限制。首先，必须在服务员作业之前定义服务员（意味着没有前向引用）。其次，如果一个作业被引用为等待者，它必须有一个唯一的名称（没有重复的等待者）。 |
| **nice**=*int*                       | 使用给定的 nice 值运行作业。在 Windows 上，小于 -15 的值将进程类设置为“高”；-1 到 -15 设置“高于正常值”；1 到 15“低于正常值”；及以上 15 个“空闲”优先级。 |
| **prio**=*int*                       | 设置此作业的 I/O 优先级值。Linux 将我们限制在 0 到 7 之间的正值，其中 0 是最高的。请参阅其他操作系统的相应联机帮助页，因为优先级的含义可能不同。对于每个命令的优先级设置，请参阅 I/O 引擎特定的 `cmdprio_percentage` 和 `cmdprio` 选项。 |
| **prioclass**=*int*                  | 设置 I/O 优先级。对于每个命令的优先级设置，请参阅 I/O 引擎特定的 `cmdprio_percentage` 和 `cmdprio_class` 选项。 |
| **cpus_allowed**=*str*               | 控制与cpumask相同的选项，但接受允许的 CPU 的文本规范，并且 CPU 从 0 开始索引。因此，要使用 CPU 0 和 5，您将指定 cpus_allowed=0,5。此选项还允许指定 CPU 范围——假设您想要绑定到 CPU 0、5 和 8 到 15，您可以设置“cpus_allowed=0,5,8-15”。<br />在 Windows 上，当 cpus_allowed 未设置时，只会使用来自 fio 的当前处理器组的 CPU，并且从系统继承关联设置。配置为面向 Windows 7 的 fio 构建提供了设置 CPU 处理器组感知的选项，并且值将设置处理器组和该组中的 CPU。例如，在处理器组 0 有 40 个 CPU 而处理器组 1 有 32 个 CPU 的系统上，0 到 39 之间的“cpus_allowed”值将绑定来自处理器组 0 的 CPU，而 40 到 71 之间的“cpus_allowed”值将绑定来自处理器的 CPU组 1. 当使用 cpus_allowed_policy=shared'时，由单个 cpus_allowed' 选项指定的所有 CPU 必须来自同一个处理器组。对于不是为 Windows 7 构建的 Windows fio 构建， |
| **cpus_allowed_policy**=*str*        | 设置 fio 如何分配**cpus_allowed**或**cpumask**指定的 CPU 的策略。支持两种策略：<br />**shared**：所有作业将共享指定的 CPU 集。<br />**split**：每个作业都会从 CPU 集中获得一个唯一的 CPU。 |
| **cpumask**=*int*                    | 设置此作业的 CPU 亲和性。给定的参数是作业可能运行的允许 CPU 的位掩码。因此，如果您希望允许的 CPU 为 1 和 5，您将传递 (1 << 1 \|1 << 5) 或 34 的十进制值。这可能不适用于所有受支持的操作系统或内核版本。此选项不适用于比您可以存储在整数掩码中的 CPU 计数更高的情况，因此它只能控制 CPU 1-32。对于 CPU 数量较多的盒子，请使用**cpus_allowed**。 |
| **numa_cpu_nodes**=*str*             | 设置此作业在指定的 NUMA 节点的 CPU 上运行。参数允许以逗号分隔的 cpu 编号列表、AB 范围或“全部”。注意，要启用 NUMA 选项支持，必须在安装了 libnuma-dev(el) 的系统上构建 fio。 |
| **numa_mem_policy**=*str*            | 设置此作业的内存策略和相应的 NUMA 节点。参数的格式：<br />                               <模式> [: <节点列表>]<br />“mode”是以下内存策略之一：“default”、“prefer”、“bind”、“interleave”或“local”。对于“默认”和“本地”内存策略，不需要指定节点。对于“prefer”，只允许一个节点。对于'bind'和'interleave'，'nodelist'可能如下：逗号分隔的数字列表、AB范围或'all'。 |
| **cgroup**=*str*                     | 将作业添加到此控制组。如果它不存在，它就将被创建。系统必须有一个挂载的 cgroup blkio 挂载点才能工作。如果您的系统没有安装它，你可以这样做：<br />               # mount -t cgroup -o blkio none /cgroup |
| **cgroup_weight**=*int*              | 将 cgroup 的权重设置为此值。请参阅内核附带的文档，允许的值在 100..1000 的范围内。 |
| **cgroup_nodelete**=*bool*           | 通常 fio 会在作业完成后删除它创建的 cgroup。要覆盖此行为并在作业完成后保留 cgroup，请设置“cgroup_nodelete=1”。如果要在作业完成后检查各种 cgroup 文件，这将很有用。默认值：假。 |
| **flow_id**=*int*                    | 流的 ID。如果未指定，则默认为全局流。                        |
| **flow**=*int*                       | 基于令牌的流量控制中的权重。如果使用此值，则 fio 调节共享相同 flow_id 的两个或多个作业之间的活动。Fio 尝试保持每个作业活动与同一 flow_id 组中其他作业的活动成比例，相对于每个作业的请求权重。也就是说，如果一个作业具有“flow=3”，另一个作业具有“flow=2”，另一个作业具有“flow=1”，那么一个作业与其他作业的运行量将大致为 3:2:1 . |
| **flow_sleep**=*int*                 | 在流计数器超过其比例后，在重试操作之前等待的时间段（以微秒为单位）。 |
| **stonewall**, **wait_for_previous** | 等待作业文件中的先前作业退出，然后再启动此作业。可用于在作业文件中插入序列化点。石墙也意味着开始一个新的报告组，请参阅**group_reporting**。或者，您可以使用 `stonewall=0` 禁用或使用 `stonewall=1` 启用它。 |
| **exitall**                          | 默认情况下，当一项作业完成时，fio 将继续运行所有其他作业。有时这不是所需的操作。设置**exitall**将使 fio 终止同一组中的所有作业，只要该组中的一项作业完成。 |
| **exit_what**=*str*                  | 默认情况下，当一项作业完成时，fio 将继续运行所有其他作业。有时这不是所需的操作。设置**exitall**将使 fio 终止同一组中的所有作业。选项**exit_what**允许您控制启用**exitall**时终止哪些作业。默认值为**组**。允许的值为：<br />**all**：终止所有作业。<br />**group**：是默认值，不会改变**exitall**的行为。<br />**stonewall**：终止所有组中所有当前正在运行的作业，并继续执行下一个阻塞组。 |
| **exec_prerun**=*str*                | 在运行此作业之前，发出通过system(3)指定的命令。输出重定向到一个名为“jobname.prerun.txt”的文件中。 |
| **exec_postrun**=*str*               | 作业完成后，发出通过system(3)指定的命令。输出重定向到一个名为“jobname.postrun.txt”的文件中。 |
| **uid**=*int*                        | 不要以调用用户身份运行，而是在线程/进程执行任何工作之前将用户 ID 设置为此值。 |
| **gid**=*int*                        | 设置组 ID。                                                  |

