# fsstress使用文档

## 一、fsstress简介

​	fsstresss为文件系统压力测试，默认添加脚本testscripts/ltpstress.sh或runltp或runalltests中。

## 二、fsstress安装

1. 进入openharmony的third_party目录,clone本仓和fsstress的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/ltp.git
   ```

2. 进行外部配置改动

   vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "hispark_taurus_standard",
     "device_company": "hisilicon",
     "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "hispark_taurus",
     "enable_ramdisk": true,
     "subsystems": [
   #############################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   #############################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   vendor/hihope/rk3568/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "device_company": "rockchip",
     "device_build_path": "device/board/hihope/rk3568",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "rk3568",
     "enable_ramdisk": true,
     "build_selinux": true,
     "subsystems": [
   ########################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   ########################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的fsstress。

## 三、编译命令

### 3.1 基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/fsstress/fsstress_master:fsstress
```

### 3.2 基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/fsstress/fsstress_master:fsstress
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：fsstress的可执行文件放入（D:\test\fsstress) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将fsstress可执行文件发送到设备端

   ```
   hdc file send D:\test\fsstress /data/test/
   ```

4. 在windows命令行上输入命令，进入板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./fsstress
   ```

## 四、fsstress使用说明

| 参数            | 含义                                                         |
| :-------------- | :----------------------------------------------------------- |
| -c              | 指定执行后不删除文件（清理）                                 |
| -d dir          | 指定操作的基目录                                             |
| -e errtg        | 指定错误注入的东西                                           |
| -f op_name=freq | 将选项名称的频率更改为freq<br/>有效的操作名称是：<br/>                        chown creat dread dwrite fdatasync fsync getdents<br/>                        链接 mkdir mknod 读取 readlink 重命名 rmdir stat<br/>                        符号链接同步截断取消链接写入 |
| -l loops        | 指定编号,测试运行应该循环的次数。使用 0 表示无限（默认 1）   |
| -n nops         | 指定编号,每个进程的操作数（默认 1）                          |
| -p nproc        | 指定编号。进程数（默认 1）                                   |
| -r              | 指定随机名称填充                                             |
| -s              | seed 指定随机生成器的种子（默认随机）                        |
| -v              | 指定详细模式                                                 |
| -w              | 将非写操作的频率归零                                         |
| -z              | 将所有操作的频率归零                                         |
| -S              | 打印操作表（省略零频率）                                     |
| -H              | 打印使用情况并退出                                           |
| -X              | 不做任何特定于 XFS 的事情（默认使用 -DNO_XFS）               |

## 五、测试示例

### 5.1fsstress在hi3516dv300的测试示例：

#### 		./fsstress [-p nproc] [-n nops] [-l loops] [-d dir] [-S]  [-v]的使用

-p 10 : 指定10个进程数

-n 1000: 每个进程操作数为1000

-l 2 :  循环次数2次

-d  /data/tmp : 指定操作基地址

-S : 打印信息

-v ：运行详细细节

**命令：**

```
./fsstress -p 10 -n 1000 -l 2 -d /data/tmp -S -v
```

**输出结果：**

**shell1终端**

```
 $ ./fsstress -p 10 -n 1000 -l 2 -d /data/tmp -S -v

4/81: rename da/c12 to da/c16 0
3/81: creat f19 x:0 0 0
3/82: dread - f13 zero size
7/123: mkdir d0/d21/d2a/d2b 0
0/101: write d8/f1c [202119,72772] 0
0/102: mknod d8/da/c1f 0
0/103: truncate d8/f1c 336362 0
0/104: symlink d8/da/d17/l20 0
8/85: write df/f18 [848886,113500] 0
3/83: dwrite f15 [1388544,61440] 0
3/84: rename fe to f1a 0
3/85: dread fd [794624,86016] 0
3/86: creat f1b x:0 0 0
2/112: chown d2/d10/l11 91944061 0
2/113: dread d2/dc/f13 [815104,126976] 0
2/114: rename d2/la to d2/l1e 0
2/115: unlink d2/dc/f12 0
8/86: mkdir df/d17/d20 0
0/105: dwrite d8/ff [2658304,53248] 0
0/106: creat d8/da/d17/f21 x:0 0 0
0/107: rmdir d8/da/d17/d1d 0
3/87: dwrite f1b [237568,28672] 0
3/88: symlink l1c 0
3/89: chown f6 1314 0
3/90: mknod c1d 0
3/91: truncate f10 287269 0
3/92: link l0 l1e 0
3/93: truncate f8 492657 0
3/94: rmdir - no directory
0/108: dwrite d8/ff [1060864,32768] 0
0/109: dread d8/da/f10 [1150976,40960] 0
0/110: stat d8/da/d17/l20 0
8/87: fsync f6 0
8/88: chown df/d15/c1b 6978 0
8/89: mknod df/c21 0
6/107: fdatasync d13/f1c 0
6/108: rmdir d13 39
8/90: read df/d17/f19 [685445,88125] 0
8/91: mknod df/d17/d20/c22 0
1/103: dread d2/fe [348160,98304] 0
9/93: dread d5/d6/f15 [4096,86016] 0
9/94: write f0 [1007890,51206] 0
9/95: symlink d5/db/l19 0
2/116: dwrite d2/dc/f13 [1253376,28672] 0
2/117: readlink d2/d10/l11 0
1/104: dwrite d2/d7/f18 [917504,77824] 0
1/105: dread d2/d7/d14/f1c [798720,102400] 0
1/106: creat d2/d7/d14/f21 x:0 0 0
1/107: truncate d2/d7/d14/f1c 860716 0
1/108: chown d2/d7/ca 410 0
1/109: chown d2/d10/d16/l1a 1023583 0
1/110: symlink d2/d7/d14/l22 0
6/109: symlink d13/l1f 0
6/110: getdents d13 0
6/111: creat d13/d15/f20 x:0 0 0

..................
```

**shell2终端(用来查看运行结果):**

```
$ ls

$ cd /data/tmp

$ ls

$ p0  p1  p2  p3  p4  p5  p6  p7  p8  p9
```

### 5.2 fsstress在rk3568的测试示例：

#### 		./fsstress [-p 10] [-n nops] [-l loops] [-d dir] [-S]  [-v]的使用

-p 10 : 指定10个进程数

-n 1000: 每个进程操作数为1000

-l 2 :  循环次数2次

-d  /data/tmp : 指定操作基地址

-S : 打印信息

-v ：运行详细细节

**命令：**

```
./fsstress -p 10 -n 1000 -l 2 -d /data/tmp -S -v
```

**输出结果：**

**shell1终端:**

```
3/114: symlink d4/d7/l23 0
2/105: dread d0/d2/f10 [20480,57344] 0
2/106: dread - d0/d2/d8/dd/f16 zero size
3/115: mknod d4/d7/d21/d12/d14/c24 0
3/116: mknod d4/c25 0
4/104: write d0/f17 [218056,103465] 0
2/107: read d0/f6 [1899572,89084] 0
4/105: mkdir d0/db/d18/d24 0
4/106: readlink d0/l1a 0
9/113: dwrite d2/de/f16 [2191360,114688] 0
6/72: fdatasync d2/f8 0
9/114: mkdir d2/de/d17/d22 0
6/73: truncate d2/fd 931136 0
6/74: readlink d2/d6/la 0
6/75: stat d2/d6/f9 0
6/76: write d2/d6/fc [1067201,15516] 0
0/106: mknod d3/da/d1f/d22/c27 0
6/77: creat d2/d6/d10/f12 x:0 0 0
0/107: truncate d3/d1c/f1e 38777 0
7/78: fdatasync d4/f16 0
7/79: stat d4/d9/lc 0
3/117: fdatasync d4/d7/f15 0
3/118: dread - d4/d7/d21/d12/d14/f1f zero size
1/99: fsync d4/f9 0
9/115: dwrite d2/de/f16 [2211840,86016] 0
6/78: dwrite d2/ff [1318912,69632] 0
6/79: chown d2/f8 1 0
7/80: creat d4/d9/dd/d14/f17 x:0 0 0
6/80: readlink d2/l4 0
6/81: getdents d2 0
6/82: chown l0 16 0
6/83: chown d2/d6/d10 30328261 0
6/84: chown d2/d6/f7 982 0
0/108: write d3/d9/f13 [764608,67946] 0
0/109: getdents d3/d1c 0
0/110: creat d3/da/d1f/f28 x:0 0 0
0/111: creat d3/da/d1f/f29 x:0 0 0
7/81: dwrite d4/f16 [978944,61440] 0
4/107: mknod d0/db/d1f/c25 0
3/119: write d4/d7/d21/f20 [1003269,93723] 0
4/108: mknod d0/db/d18/d23/c26 0
7/82: dwrite d4/d9/dd/d14/f17 [659456,65536] 0
3/120: unlink d4/d7/d21/l19 0
4/109: dread d0/da/f3 [1548288,12288] 0
5/115: sync
1/100: rename d4/dc/d1b to d4/d1f 0
5/116: stat d1/fe 0
1/101: readlink d4/dc/l18 0
5/117: dread - d1/dd/d13/f1a zero size
1/102: link d4/dc/f17 d4/f20 0
8/96: sync
8/97: dread d4/fb [634880,61440] 0
5/118: mkdir d1/dd/d17/d27 0
7/83: truncate d4/f11 721704 0
5/119: symlink d1/dd/d17/d1c/d23/l28 0
8/98: write d4/fb [1694893,118785] 0
5/120: readlink d1/l16 0
8/99: chown d4/de/d1b 14 0
5/121: truncate d1/fa 817216 0
8/100: mknod d4/de/df/c1d 0
9/116: dwrite d2/f13 [2007040,126976] 0
8/101: mknod d4/de/c1e 0
9/117: link l1 d2/l23 0
9/118: chown d2/de/d17/c1e 4799453 0
9/119: readlink d2/l18 0
8/102: dread d4/f11 [53248,118784] 0
8/103: stat d4/c9 0
9/120: symlink d2/de/d17/l24 0
8/104: creat d4/d10/f1f x:0 0 0
7/84: dwrite d4/f5 [4259840,110592] 0
7/85: creat d4/d9/dd/f18 x:0 0 0
5/122: dwrite d1/f9 [1449984,61440] 0
5/123: creat d1/f29 x:0 0 0
5/124: rmdir d1 39
4/110: mkdir d0/db/d18/d23/d27 0
5/125: stat d1/dd 0
8/105: write d4/d10/f19 [478920,104696] 0
4/111: dread - d0/da/f1c zero size
5/126: write d1/fe [941731,58725] 0
4/112: creat d0/f28 x:0 0 0
4/113: creat d0/db/d18/d23/d27/f29 x:0 0 0
5/127: symlink d1/dd/d17/d1c/d23/l2a 0
5/128: dread - d1/dd/d17/d1c/f20 zero size
5/129: mknod d1/dd/d13/c2b 0
7/86: dwrite d4/d9/dd/d14/f17 [1753088,94208] 0
5/130: mkdir d1/d2c 0
7/87: rename d4/d9/lb to d4/d9/dd/l19 0
7/88: mknod d4/d9/c1a 0
7/89: stat d4/d9 0
1/103: dwrite d4/dc/fd [81920,61440] 0
9/121: dwrite d2/de/f16 [3284992,12288] 0
5/131: dwrite d1/f2 [335872,32768] 0
7/90: dwrite d4/d9/dd/f18 [53248,36864] 0
5/132: symlink d1/dd/d13/l2d 0
8/106: dwrite d4/f5 [40960,24576] 0
6/85: fsync d2/f8 0
7/91: read d4/d9/dd/f18 [5110,99358] 0
7/92: rmdir d4/d9/dd 39
3/121: link d4/d7/ld d4/d7/d21/d12/l26 0
6/86: truncate d2/d6/d10/f12 455287 0
2/108: sync
2/109: dread d0/d2/d5/fa [229376,65536] 0
7/93: write d4/f16 [1508164,91622] 0
7/94: rename d4/c7 to d4/c1b 0
7/95: creat d4/d9/dd/f1c x:0 0 0
6/87: dwrite d2/d6/f7 [1851392,12288] 0
7/96: write d4/d9/dd/d14/f17 [1850597,29801] 0
7/97: truncate d4/f16 1859460 0
6/88: dread d2/d6/d10/f12 [249856,86016] 0
7/98: write d4/f16 [1619444,91470] 0
7/99: rename d4/d9/lc to d4/d9/dd/d14/l1d 0
7/100: truncate d4/f5 4765694 0
3/122: unlink d4/d7/d21/l10 0
7/101: mkdir d4/d9/dd/d1e 0
6/89: fsync d2/d6/f9 0
7/102: link d4/d9/dd/c15 d4/d9/dd/c1f 0
7/103: creat d4/d9/dd/d1e/f20 x:0 0 0
6/90: write d2/d6/f9 [1648527,11661] 0
7/104: creat d4/f21 x:0 0 0
8/107: dwrite d4/f11 [200704,86016] 0
9/122: fsync d2/f13 0
7/105: write d4/d9/dd/d1e/f20 [558902,107028] 0
7/106: rmdir d4/d9 39
7/107: creat d4/d9/dd/f22 x:0 0 0
7/108: write d4/f5 [1438287,97603] 0
7/109: creat d4/f23 x:0 0 0
7/110: mkdir d4/d9/dd/d14/d24 0
7/111: readlink l2 0
7/112: readlink d4/l13 0
5/133: fsync d1/f9 0
5/134: dread d1/fa [405504,110592] 0
4/114: sync
9/123: dwrite d2/f1f [94208,4096] 0
9/124: rmdir d2/de/d17 39
9/125: rmdir d2/de/d17 39
0/112: sync
9/126: write d2/f13 [329979,62102] 0
9/127: stat d2/de/d17/l21 0
9/128: symlink d2/de/d17/l25 0
0/113: fdatasync d3/da/db/f24 0
9/129: creat d2/d19/f26 x:0 0 0
9/130: creat d2/de/f27 x:0 0 0
0/114: mkdir d3/da/d1f/d22/d2a 0
0/115: stat d3/da/d1f/d22/d2a 0
3/123: write d4/d7/d21/d12/d14/f1f [590837,112275] 0
9/131: creat d2/de/d17/f28 x:0 0 0
0/116: creat d3/d9/f2b x:0 0 0
2/110: write d0/f6 [3122995,73648] 0
0/117: write d3/da/db/f1b [2345490,13060] 0
6/91: mknod d2/d6/c13 0
6/92: truncate d2/fd 1730101 0
6/93: readlink d2/d6/la 0
6/94: truncate d2/fd 2073605 0
2/111: getdents d0/d2/df 0
8/108: chown d4/d18 57834 0
2/112: truncate d0/d2/d8/dd/f16 119431 0
9/132: dread d2/de/f1b [499712,57344] 0
9/133: stat d2/de/d17/l1c 0
4/115: creat d0/db/d18/d23/f2a x:0 0 0
1/104: sync
4/116: mknod d0/db/d18/d23/d27/c2b 0
2/113: mkdir d0/d2/d8/d17 0
4/117: creat d0/f2c x:0 0 0
2/114: dread d0/d2/d5/d13/f12 [192512,16384] 0
4/118: mkdir d0/db/d2d 0
5/135: dwrite d1/dd/d13/f1a [208896,122880] 0
4/119: write d0/da/f16 [335914,73577] 0
4/120: read - d0/f28 zero size
4/121: getdents d0/db/d18/d23/d27 0
5/136: mknod d1/dd/d17/d27/c2e 0
4/122: dread d0/f19 [2244608,61440] 0
5/137: dread d1/fa [679936,61440] 0
5/138: dread - d1/dd/f25 zero size
5/139: dread - d1/dd/d17/d1c/f20 zero size
4/123: creat d0/db/d18/d24/f2e x:0 0 0
5/140: chown d1/l7 24 0
4/124: rmdir d0/db/d1f 39
9/134: mknod d2/de/d17/c29 0
8/109: dwrite d4/f5 [1175552,57344] 0
8/110: symlink d4/de/df/l20 0
4/125: dwrite d0/f28 [749568,57344] 0
6/95: fdatasync d2/d6/f7 0
3/124: dwrite d4/d7/f18 [331776,106496] 0
5/141: dwrite d1/f29 [638976,53248] 0
8/111: mkdir d4/de/d21 0
8/112: creat d4/d10/f22 x:0 0 0
8/113: chown d4/d18/c1a 4 0
9/135: dwrite d2/f1f [802816,12288] 0
9/136: stat d2/l23 0
9/137: stat d2/d19/f26 0
9/138: write d2/de/d17/f28 [651435,46572] 0
9/139: truncate d2/f13 2409763 0
9/140: write d2/de/d17/f28 [921480,17250] 0
9/141: creat d2/d19/f2a x:0 0 0
9/142: dread d2/f1f [241664,20480] 0
9/143: chown d2/f1f 20715 0
9/144: chown d2/de/f27 4043676 0
9/145: write d2/fb [1387806,127710] 0
9/146: chown d2/de/d17/l1c 35423 0
4/126: symlink d0/db/d1f/l2f 0
8/114: getdents d4/de 0
5/142: symlink d1/dd/d17/l2f 0
5/143: rename d1/dd/d17/d1c/f20 to d1/dd/d13/f30 0
1/105: creat d4/dc/f21 x:0 0 0
7/113: dwrite d4/f11 [851968,12288] 0
7/114: truncate d4/d9/dd/f18 423078 0
5/144: chown d1/dd/d17/f1f 179375 0
5/145: mknod d1/dd/d17/d1c/d23/c31 0
9/147: unlink d2/l6 0
9/148: mkdir d2/de/d17/d2b 0
1/106: dread d4/f9 [208896,118784] 0
8/115: dwrite d4/d10/f16 [1019904,20480] 0
1/107: creat d4/f22 x:0 0 0
8/116: chown d4/f6 1550242 0
1/108: link d4/f15 d4/f23 0
1/109: rmdir d4 39
8/117: creat d4/d18/f23 x:0 0 0
7/115: dwrite d4/f23 [757760,32768] 0
4/127: dwrite d0/db/d18/d23/f2a [204800,102400] 0
4/128: creat d0/da/f30 x:0 0 0
1/110: write d4/f23 [1564135,90991] 0
7/116: truncate d4/f23 773953 0
7/117: chown c1 10569124 0
7/118: chown l2 292772 0
0/118: sync
0/119: chown d3/d1c 388 0
7/119: fsync d4/d9/dd/f22 0
7/120: chown d4/d9/dd/d14/c10 241 0
4/129: fsync d0/da/f9 0
4/130: dread - d0/db/d18/f1e zero size
0/120: read d3/da/f10 [35790,64810] 0
9/149: dwrite d2/de/d17/f28 [864256,53248] 0
9/150: stat d2/de/d17/d22 0
0/121: creat d3/f2c x:0 0 0
0/122: getdents d3/da/d1f 0
7/121: creat d4/d9/dd/d1e/f25 x:0 0 0
7/122: rmdir d4/d9/dd 39
1/111: dwrite d4/dc/f21 [532480,12288] 0
9/151: unlink d2/l10 0
4/131: dwrite d0/da/f3 [1740800,114688] 0
4/132: creat d0/db/f31 x:0 0 0
7/123: fdatasync d4/d9/dd/f1c 0
1/112: fsync d4/f19 0
0/123: write d3/da/db/f26 [667269,128728] 0
9/152: fsync d2/de/f1b 0
9/153: mkdir d2/d2c 0
4/133: symlink d0/db/d18/d23/l32 0
4/134: dread - d0/da/f30 zero size
3/125: sync
3/126: chown d4/d7/d21/d12/l26 0 0
3/127: stat d4/d7/d21/d12 0
..............
```

**shell2**

```
$ ls

$ cd /data/tmp

$ ls

$ p0  p1  p2  p3  p4  p5  p6  p7  p8  p9
```
