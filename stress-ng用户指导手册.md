# Stress-ng使用手册

## 一、stress-ng简介

​	stress-ng将以各种可选择的方式对计算机系统进行压力测试。它被设计用来运行计算机的各种物理子系统以及各种操作系统内核接口。stress-ng还具有广泛的特定于CPU的压力测试，用于执行浮点、整数、位操作和控制流。

​	stress-ng最初的目的是让机器更努力地工作，避免硬件问题，如热溢出和操作系统bug，这些问题只在系统受到严重冲击时才会发生。谨慎使用stress-ng，因为一些测试可能会使系统在设计不良的硬件上运行热，还可能导致难以停止的过度系统抖动。

​	stress-ng还可以测量测试吞吐率;这对于观察不同操作系统版本或硬件类型的性能变化非常有用。然而，它从来没有打算用作精确的基准测试套件，所以不要以这种方式使用它。
​	在Linux系统中使用root权限运行stress-ng将调整内存设置，使压力源在内存不足的情况下无法消除，所以要明智地使用它。使用适当的特权，stress-ng可以允许调整ionice类和ionice级别，同样，应该谨慎使用。

## 二、stress-ng安装

1. 进入openharmony的third_party目录,clone本仓和fsstress的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/stress-ng.git
   ```

2. 进行外部配置改动

   vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "hispark_taurus_standard",
     "device_company": "hisilicon",
     "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "hispark_taurus",
     "enable_ramdisk": true,
     "subsystems": [
   #############################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   #############################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   vendor/hihope/rk3568/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "device_company": "rockchip",
     "device_build_path": "device/board/hihope/rk3568",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "rk3568",
     "enable_ramdisk": true,
     "build_selinux": true,
     "subsystems": [
   ########################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   ########################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的stress-ng。

## 三、编译命令

### 3.1 基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/stress_ng/stress_ng_master:stress_ng
```

### 3.2基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/stress-ng/stress-ng_master:stress-ng
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：stress-ng的可执行文件放入（D:\test\stress-ng) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data/test
   ```

3. 在windows命令行上输入命令，从host端将stress-ng可执行文件发送到设备端

   ```
   hdc file send D:\test\stress-ng /data/test/
   ```

4. 在windows命令行上输入命令，进入板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./stress-ng
   ```

## 四、stress-ng常用参数

| 参数             | 用法                                                         |
| ---------------- | ------------------------------------------------------------ |
| -c N             | 生成N个worker循环调用sqrt()产生cpu压力                       |
| -i N             | 生成N个worker循环调用sync()产生io压力                        |
| -m N             | 生成N个worker循环调用malloc()/free()产生内存压力             |
| -c N             | 运行N worker CPU压力测试进程                                 |
| --cpu-method all | worker从迭代使用30多种不同的压力算法，包括pi, crc16, fft等等 |
| -tastset N       | 将压力加到指定核心上                                         |
| -d N             | 运行N worker HDD write/unlink测试                            |
| -i N             | 运行N worker IO测试                                          |

## 五、测试实例

### 5.1在hi3516dv300上测试示例

#### 5.1.1./stress-ng --cpu N --vm N --hdd N --fork N --timeout T --metrics的使用

--cpu N:代表进程个数（每个进程会占用一个cpu，当超出cpu个数时，进程间会互相争用cpu）

--vm N: 在mmap上的压力个数

--hdd N: N个在write()/unlink()的压力

--fork N: N个在fork()和exit()上的压力

--timeout：表示测试时长

--metrics: 打印活动的伪指标

**命令：**

```
./stress-ng --cpu 4 --vm 2 --hdd 1 --fork 8 --timeout 2m --metrics
```

**结果**

```
stress-ng: info:  [7316] setting to a 120 second (2 mins, 0.00 secs) run per stressor
stress-ng: info:  [7316] dispatching hogs: 4 cpu, 2 vm, 1 hdd, 8 fork
stress-ng: info:  [7316] successful run completed in 123.07s (2 mins, 3.07 secs)
stress-ng: info:  [7316] stressor       bogo ops real time  usr time  sys time   bogo ops/s     bogo ops/s CPU used per
stress-ng: info:  [7316]                           (secs)    (secs)    (secs)   (real time) (usr+sys time) instance (%)
stress-ng: info:  [7316] cpu                 734    121.00     10.24      0.05         6.07          71.33         2.13
stress-ng: info:  [7316] vm                 7009    121.49      0.00      0.00        57.69           0.00         0.00
stress-ng: info:  [7316] hdd                2136    122.51      1.38      1.34        17.44         785.29         2.22
stress-ng: info:  [7316] fork               9577    120.11      0.56      5.55        79.74        1567.43         0.64
```

### 5.2 在RK3568上测试示例

#### 5.2.1 ./stress-ng --cpu N --vm N --hdd N --fork N --timeout T --metrics的使用

--cpu N:代表进程个数（每个进程会占用一个cpu，当超出cpu个数时，进程间会互相争用cpu）

--vm N: 在mmap上的压力个数

--hdd N: N个在write()/unlink()的压力

--fork N: N个在fork()和exit()上的压力

--timeout：表示测试时长

--metrics: 打印活动的伪指标

**命令：**

```
./stress-ng --cpu 4 --vm 2 --hdd 1 --fork 8 --timeout 2m --metrics
```

**输出结果：**

```
stress-ng: info:  [1797] setting to a 120 second (2 mins, 0.00 secs) run per stressor
stress-ng: info:  [1797] dispatching hogs: 4 cpu, 2 vm, 1 hdd, 8 fork
stress-ng: info:  [1797] stressor       bogo ops real time  usr time  sys time   bogo ops/s     bogo ops/s CPU used per
stress-ng: info:  [1797]                           (secs)    (secs)    (secs)   (real time) (usr+sys time) instance (%)
stress-ng: info:  [1797] cpu               30280    120.04    158.50      1.68       252.25         189.04        33.36
stress-ng: info:  [1797] vm              1206557    120.07      0.00      0.00     10048.76           0.00         0.00
stress-ng: info:  [1797] hdd               79629    120.45     14.22     24.84       661.09        2038.63        32.43
stress-ng: info:  [1797] fork             167734    120.00      7.53     82.57      1397.77        1861.64         9.39
stress-ng: info:  [1797] successful run completed in 120.46s (2 mins, 0.46 secs)
```
