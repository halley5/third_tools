

# fio使用文档

## 一、fio简介

​		fio是一种I / O工具，用于基准测试和压力/硬件验证。它支持19种不同类型的I / O引擎(sync，mmap，libaio，posixaio，SG v3，splice，null，network，syslet，guasi，solarisaio等)，I / O优先级(适用于较新的Linux内核) ，评估I / O，分叉或线程作业等等。它可以在块设备和文件上工作。fio以简单易懂的文本格式接受职位描述。包含几个示例作业文件。fio显示各种I / O性能信息，包括完整的IO延迟和百分位数。Fio广泛用于许多地方，包括基准测试，QA和验证目的。它支持Linux，FreeBSD，NetBSD，OpenBSD，OS X，OpenSolaris，AIX，HP-UX，Android和Windows。

## 二、fio安装

1. ​	进入openharmony的third_party目录,clone本仓和fio的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/fio.git
   ```

2. 进行外部配置改动

   productdefine/common/products目录Hi3516DV300.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "Hi3516DV300",
     "product_company": "hisilicon",
     "product_device": "hi3516dv300",
     "version": "2.0",
     "type": "standard",
     "product_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "parts":{
       "ace:ace_engine_standard":{},
       "ace:napi":{},
   ##############################################
       "third_tools:tools":{},
   ##############################################
       "account:os_account_standard":{},
       "distributeddatamgr:native_appdatamgr":{},
   ```

   productdefine/common/products目录rk3568.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "product_company": "hihope",
     "product_device": "rk3568",
     "version": "2.0",
     "type": "standard",
     "product_build_path": "device/board/hihope/rk3568",
     "parts":{
       "ace:ace_engine_standard":{},
       "ace:napi":{},
   ################################################
       "third_tools:tools":{},
   ################################################
       "account:os_account_standard":{},
       "barrierfree:accessibility":{},
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的fio。

## 三、编译命令

### 3.1基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/third_tools/fio:fio
```

### 3.2基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/third_tools/fio:fio
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：fio的可执行文件放入（D:\test\fio) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将fio可执行文件发送到设备端

   ```
   hdc file send D:\test\fio /data/test/
   ```

4. 在windows命令行上输入命令，进行板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./fio
   ```

## 四、fio中的参数

​	fio是可以根据测试要求配置不同参数进行io性能测试的工具，可配置的参数有上百个，几乎可以覆盖所有的io模型，详细参数可查询fio的官网获取：（[https://fio.readthedocs.io/en/latest/fio_doc.html](https://bbs.huaweicloud.com/forum/forum.php?mod=viewthread&tid=90290#)）。存储性能测试中常用的参数如下所示。

### 4.1主要的参数

| 参数                  | 参数值                                                       | 解释                                                         |
| --------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| filename              | 设备名或文件名，如：裸设备-/dev/sdb，文件-/home/test.img     | 定义测试对象，一般是设备或者文件。如果想测试裸盘设备/dev/sdb，可以设置filename=/dev/sdb；如果想要测试文件系统性能，则可以设置filename=/home/test.img |
| name                  | 测试名称                                                     | 定义测试名称。必填项，本地fio测试的名称，对性能没有影响。    |
| rw                    | 测试类型，可选值有：read，write，rw，randread，randwrite，randrw | 定义测试的读写类型：read-顺序读，write-顺序写，rw(readwrite)- 混合顺序读写，randread- 随机读，randwrite-随机写，randrw-混合随机读写。对于读写混合类型来说，默认读写比例为1:1 |
| rwmixwrite，rwmixread | 混合读写中读写占用比例，可选值为[0,100]                      | 定义在混合读写模式中，写或读所占的比例。举例来说，rwmixread值为10，则表示读写比为10:90。 |
| ioengine              | io引擎选择，可选值有sync、libaio、psync、vsync、mmap等等     | 定义fio如何下发io请求。sync：基本的read、write io。 libaio：linux原生的异步io，linux只支持non-buffer情况下的排队操作。默认值为psync，该模式为同步io模型，异步io模型libaio，一般结合direct=1使用。 |
| direct                | 0或1                                                         | 定义是否使用direct io：值为0，表示使用buffered io；值为1，表示使用direct io。 |
| bs                    | 带单位数字                                                   | 定义io的块大小，单位是k，K，m，M。默认值为4k。               |
| numjobs               | 正整数                                                       | 定义测试的进程/线程数，默认值为1，如果指定了thread参数，则使用单进程多线程模式，否则使用多进程模式。 |
| iodepth               | 正整数                                                       | 定义每个进程/线程可以同时下发的io任务数，默认为1，适用于异步io场景，同步io场景要等前一个io任务完成才能下发下一个任务，所以iodepth并不起作用。 |

其他相关参数：

| 参数            | 参数值           | 解释                                                         |
| --------------- | ---------------- | ------------------------------------------------------------ |
| size            | 整数             | 定义io操作的数据量，除非指定了runtime这个参数，fio会将指定大小的数据量全部读写完成，才会停止测试。该参数的值，可以是带单位的数字，比如size=2G，表示读/写的数据量为2G；也可以是百分比，比如size=20%，表示读写的数据量占该设备/文件的20%的空间；除了表示io操作数据量，size还表示io操作的范围，与offset配合一起读写[offset,offset+size]范围内的数据。 |
| runtime         | 正整数           | 定义测试时间，以秒为单位。对于指定的文件设备，如果在测试时间内完成了读写操作，则会保持相同的负载循环进行读写。 |
| ramp_time       | 正整数           | 定义测试的热身时间，以秒为单位。热身时间不计入测试统计。     |
| thinktime       | 正整数           | 设置当一个io完成后，等待多少时间再下发另一个io，单位为微妙。一般用于模拟应用等待、处理事务等。 |
| time_based      | NA               | 默认值为0，如果设置了该参数，则本次测试会一直运行到runtime结束为止。 |
| offset          | 定义测试起始位置 | fio在硬盘测试的起始位置，配合顺序读写使用。不同的offset对HDD的性能影响大，对SSD理论无影响。 |
| group_reporting | NA               | 对于测试并发数大于1的情况，如果想要将所有进程/线程的测试结果进行统计，输出总的测试结果，而不是各自单独输出，可以使用此参数。 |
| cpus_allowed    | 指定cpu的core    | fio测试的所有进程/线程可以选择的cpu核，可以是单独一个或者多个核，也可以是一个范围。 |
| output          | 结果输出路径     | 结果输出文件，默认直接把结果打印到命令行，设置后会把结果写进目标文件，命令行则不显示结果。 |

使用参数：

| 参数                 | 含义                                                   |
| -------------------- | ------------------------------------------------------ |
| --debug=options      | 启用调试日志记录。可能是一个/多个:                     |
| --parse-only         | 仅解析选项，不启动任何IO                               |
| --bandwidth-log      | 生成聚合带宽日志                                       |
| --minimal            | 最小(简洁)输出                                         |
| --output-format=type | 输出格式(简体，json,json+，正常)                       |
| --terse-version=type | 设置简洁版本输出格式(默认为3,2或4)                     |
| --version            | 打印版本信息并退出                                     |
| --help               | 打印帮助文档                                           |
| --cpuclock-test      | 测试/验证CPU时钟                                       |
| --crctest=[type]     | 校验和函数的测试速度                                   |
| --cmdhelp=cmd        | 打印所有的命令帮助，"all"                              |
| --enghelp=engine     | 打印ioengine帮助，或列出可用的ioengine                 |
| --enghelp=engine,cmd | 打印ioengine cmd的帮助                                 |
| --showcmd            | 将工作文件转换为命令行选项                             |
| --eta=when           | eta估计应该打印<br/>可能是"always"， "never"或"auto"   |
| --eta-newline=t      | 每经过一个't'句点就强制一个新行                        |
| --status-interval=t  | 每过't'周期强制转储全状态                              |
| --readonly           | 开启安全只读检查，防止写入                             |
| --section=name       | 只有在工作文件中运行指定的section，才能指定多个section |
| --alloc-size=kb      | 设置smallc pool的大小为kb (def 16384)                  |
| --warnings-fatal     | Fio解析器的警告是致命的                                |
| --max-jobs=nr        | 要支持的最大线程/进程数                                |
| --server=args        | 启动后端fio服务器                                      |
| --daemonize=pidfile  | 后台fio服务器，将pid写入文件                           |
| --client=hostname    | 在主机名处与远程后端fio服务器对话                      |
| --remote-config=file | 告诉fio服务器加载这个本地工作文件                      |
| --idle-prof=option   | 报告系统或每个cpu的空闲度                              |
| --trigger-file=file  | 文件存在时执行触发器cmd                                |
| --trigger-timeout=t  | 此时执行触发器                                         |
| --trigger=cmd        | 设置此命令为本地触发器                                 |
| --trigger-remote=cmd | 设置此命令为远程触发器                                 |
| --aux-path=path      | 用于生成文件的fio状态                                  |

## 五、测试示例

### 5.1fio在hi3516dv300的测试示例：

#### 5.1.1fio [-name]  [-filename] [-direct N]  [-iodepth=N]  [-thread] [-rw]  [-ioengine]  [-bs=N]  [-size=N]  [-numjobs=N]  [-runtime=N] [-group_reporting]

#### 测试1：

```
./fio -name=mytest -directory=fiotest -direct=1 -iodepth=20 -thread -rw=read -ioengine=null -bs=16k -size=500M -numjobs=2 -runtime=300 -group_reporting
```

name=mytest                 本次测试的名称mytest，自已定义。
filename                          测试裸盘的名称，通常选择需要测试的盘的data目录，是没有文件系统的裸盘。directory: 设置filename的路径前缀。
direct=1                           测试过程绕过机器自带的buffer。使测试结果更真实。
iodepth=20                     每个线程每次下发20个io任务。
rw=read              			测试读的I/O。
ioengine=libaio         	 io引擎使用libaio模式。
bs=16k                  		  单次io的块文件大小为16k。
size=5G                           本次的测试文件大小为5G，以每次16k的io进行测试。
numjobs=2                     本次的测试线程为2。
runtime=300                  测试时间为300秒，如果不写则一直将5G文件分16k每次写完为止。
group_reporting            关于显示结果的，汇总每个进程的信息。

#### 测试结果：

```
FIO_VERSION
Starting 2 threads
Jobs: 2 (f=2): [R(2)][100.0%][r=22.7MiB/s][r=1453 IOPS][eta 00m:00s]
mytest: (groupid=0, jobs=2): err= 0: pid=2078: Thu Jan  1 21:15:38 1970
  read: IOPS=1444, BW=22.6MiB/s (23.7MB/s)(1000MiB/44298msec)
    slat (usec): min=22, max=7521, avg=36.31, stdev=45.11
    clat (usec): min=1953, max=58344, avg=27622.75, stdev=1290.04
     lat (usec): min=2050, max=58443, avg=27662.83, stdev=1288.58
    clat percentiles (usec):
     |  1.00th=[26870],  5.00th=[27395], 10.00th=[27395], 20.00th=[27395],
     | 30.00th=[27657], 40.00th=[27657], 50.00th=[27657], 60.00th=[27657],
     | 70.00th=[27657], 80.00th=[27657], 90.00th=[27657], 95.00th=[27919],
     | 99.00th=[30278], 99.50th=[34341], 99.90th=[46400], 99.95th=[48497],
     | 99.99th=[52691]
   bw (  KiB/s): min=21845, max=24248, per=100.00%, avg=23137.23, stdev=201.09, samples=176
   iops        : min= 1365, max= 1515, avg=1445.56, stdev=12.57, samples=176
  lat (msec)   : 2=0.01%, 10=0.02%, 20=0.35%, 50=99.60%, 100=0.03%
  cpu          : usr=2.84%, sys=6.25%, ctx=24938, majf=0, minf=326
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.1%, 64=0.0%, >=64=0.0%
     issued rwts: total=64000,0,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=20

Run status group 0 (all jobs):
   READ: bw=22.6MiB/s (23.7MB/s), 22.6MiB/s-22.6MiB/s (23.7MB/s-23.7MB/s), io=1000MiB (1049MB), run=44298-44298msec

Disk stats (read/write):
  mmcblk0: ios=5951/48, merge=57778/38, ticks=163690/1300, in_queue=165100, util=99.95%
```



### 5.2fio在rk3516的测试示例：

#### 5.1.1fio [-name]  [-filename] [-direct N]  [-iodepth=N]  [-thread] [-rw]  [-ioengine]  [-bs=N]  [-size=N]  [-numjobs=N]  [-runtime=N] [-group_reporting]

#### 测试2：

```
./fio -name=mytest -directory=fiotest -direct=1 -iodepth=20 -thread -rw=randread -ioengine=libaio -bs=16k -size=500M -numjobs=2 -runtime=300 -group_reporting
```

name=mytest                 本次测试的名称mytest，自已定义。
filename                          测试裸盘的名称，通常选择需要测试的盘的data目录，是没有文件系统的裸盘。directory: 设置filename的路径前缀。
direct=1                           测试过程绕过机器自带的buffer。使测试结果更真实。
iodepth=20                     每个线程每次下发20个io任务。
rw=randread              	测试随机读的I/O。
ioengine=libaio         	 io引擎使用libaio模式。
bs=16k                  		  单次io的块文件大小为16k。
size=5G                           本次的测试文件大小为5G，以每次16k的io进行测试。
numjobs=2                     本次的测试线程为2。
runtime=300                  测试时间为300秒，如果不写则一直将5G文件分16k每次写完为止。
group_reporting            关于显示结果的，汇总每个进程的信息。

#### **测试结果：**

```
mytest: (g=0): rw=randread, bs=(R) 16.0KiB-16.0KiB, (W) 16.0KiB-16.0KiB, (T) 16.0KiB-16.0KiB, ioengine=libaio, iodepth=20
...
FIO_VERSION
Starting 2 threads
mytest: Laying out IO file (1 file / 500MiB)
mytest: Laying out IO file (1 file / 500MiB)
Jobs: 1 (f=1): [r(1),_(1)][96.2%][r=41.9MiB/s][r=2684 IOPS][eta 00m:01s]
mytest: (groupid=0, jobs=2): err= 0: pid=1747: Sat Aug  5 10:00:12 2017
  read: IOPS=2514, BW=39.3MiB/s (41.2MB/s)(1000MiB/25452msec)
    slat (usec): min=48, max=1367, avg=164.98, stdev=53.96
    clat (usec): min=979, max=33497, avg=15318.06, stdev=6021.51
     lat (usec): min=1119, max=33649, avg=15495.87, stdev=6022.96
    clat percentiles (usec):
     |  1.00th=[ 2802],  5.00th=[ 5080], 10.00th=[ 7046], 20.00th=[10159],
     | 30.00th=[12387], 40.00th=[14091], 50.00th=[15270], 60.00th=[16450],
     | 70.00th=[17957], 80.00th=[20317], 90.00th=[23987], 95.00th=[26084],
     | 99.00th=[28443], 99.50th=[29230], 99.90th=[30278], 99.95th=[30802],
     | 99.99th=[31327]
   bw (  KiB/s): min=38386, max=64389, per=100.00%, avg=40832.26, stdev=2668.31, samples=98
   iops        : min= 2398, max= 4024, avg=2551.53, stdev=166.77, samples=98
  lat (usec)   : 1000=0.01%
  lat (msec)   : 2=0.28%, 4=2.50%, 10=16.75%, 20=59.75%, 50=20.71%
  cpu          : usr=20.00%, sys=58.51%, ctx=75744, majf=0, minf=330
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.1%, 64=0.0%, >=64=0.0%
     issued rwts: total=64000,0,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=20

Run status group 0 (all jobs):
   READ: bw=39.3MiB/s (41.2MB/s), 39.3MiB/s-39.3MiB/s (41.2MB/s-41.2MB/s), io=1000MiB (1049MB), run=25452-25452msec

Disk stats (read/write):
  mmcblk0: ios=63163/28, merge=582/25, ticks=970545/570, in_queue=971131, util=99.79%
```



## 六、注意事项

### 6.1测试注意事项

1. fio测试会对系统的硬盘进行读写，会存在破坏客户数据的风险，因此在生产环境上要谨慎使用，不建议对生产环境的硬盘进行裸盘读写测试。
2. 如果需要进行测试，最好创建一个文件，然后对指定的文件进行读写。
3. 如果需要做性能分析或调优，测试过程中需要收集iostat信息，例如：iostat -xd 1 1000 > result.log

### 6.2性能调优注意事项

1. FIO进行性能对比测试的时候，一定要保持参数一致，否则参数差异会导致结果差异。
2. 在测试文件系统时，要先写一个大文件，然后再做硬盘IO读写测试。避免小数据块IO模型下读取不到文件而读到其他地方去。
3. 如果是SSD，测试过程中最好进行绑核设置，因为IOPS太大，如果线程运行在CPU0或者是有其他应用占用的核，那么可能会达不到最佳性能。绑核使用taskset -c 31 ./fio…命令实现。对应使用哪个核，可以执行numactl -H查询核的信息。