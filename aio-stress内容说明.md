# aio-stress内容说明

## 一、介绍

aio-stress相关内容位于https://gitee.com/halley5/ltp.git

基于https://github.com/linux-test-project/ltp.git

该仓为ltp仓，其中包含fsstress和aio，当前二者均已完成

## 二、主要修改

主要修改了一个BUILD.gn文件，添加config.h文件

BUILD.gn内容：

```
import("//build/ohos.gni")

executable("fsstress"){
    sources =[
        "testcases/kernel/fs/fsstress/fsstress.c"
    ]

    include_dirs = [
        "testcases/kernel/fs/fsstress",
        "include"
    ]

    deps = [
        "//third_party/musl:musl_all"
    ]
}
executable("aio"){
    sources =[
        "testcases/kernel/io/ltp-aiodio/aio-stress.c",
    ]

    include_dirs = [
        "//third_party/third_tools/libaio_0.3.113/src",
        "lib",
        "include",
        "include/old",
    ]

    deps = [
        "//third_party/musl:musl_all",
        "//third_party/third_tools/libaio_0.3.113:libaio"
    ]
}
```

对比视图：https://gitee.com/halley5/ltp/commit/5d82c9e9b6d6bba2b3c12ef2c753559c3e2e74ab
