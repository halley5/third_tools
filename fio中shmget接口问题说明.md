# fio中shmget接口问题说明

#### 	基于最新镜像的rk3568在进行fio测试时出现signal 31程序退出:

```
#./fio -name=mytest -directory=fiotest -direct=1 -iodepth=20 -thread -rw=read -ioengine=libaio -bs=16k -size=500M -numjobs=2 -runtime=30 -group_reporting
Signal 31
```

#### 	通过打印追踪之后发现为musl中的shmget接口中的syscall出现signal 31：

```
#ifndef CONFIG_NO_SHM
	seg->shm_id = shmget(0, size, IPC_CREAT | 0600);
	if (seg->shm_id == -1) {
		if (errno != EINVAL && errno != ENOMEM && errno != ENOSPC)
			perror("shmget");
		return -1;
	}
#else
	seg->threads = malloc(size);
	if (!seg->threads)
		return -1;
#endif
```

```
int shmget(key_t key, size_t size, int flag)
{
	if (size > PTRDIFF_MAX) size = SIZE_MAX;
#ifndef SYS_ipc
	return syscall(SYS_shmget, key, size, flag);
#else
	return syscall(SYS_ipc, IPCOP_shmget, key, size, flag);
#endif
}
```

#### 	社区中已有人在musl库中提issues：https://gitee.com/openharmony/third_party_musl/issues/I5MQ9A?from=project-issue，但是此缺陷暂未解决。

#### 	所以我们在rk3568上进行fio的测试时，均使用的是五月末的镜像。

