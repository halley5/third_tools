# FIO的nfs问题说明

nfs引擎问题

使用命令```./fio -name=mytest -directory=nfs -direct=1 -iodepth=20 -thread -rw=read -ioengine=nfs -nfs_url=nfs://192.168.1.11/nfs -bs=16k -size=500M -numjobs=2 -runtime=300 -group_reporting```时log信息如下

```
mytest: (g=0): rw=read, bs=(R) 16.0KiB-16.0KiB, (W) 16.0KiB-16.0KiB, (T) 16.0KiB-16.0KiB, ioengine=nfs, iodepth=20
...
FIO_VERSION
Starting 2 threads
Jobs: 0 (f=0)
^Z[1] + Signal 20            ./fio -name=mytest -directory=nfs -direct=1 -iodepth=20 -thread -rw=read -ioengine=nfs -nfs_url=nfs://192.168.1.11/nfs -bs=16k -size=500M -numjobs=2 -runtime=300 -group_reporting
```

陷入阻塞状态

追踪代码发现与正确运行对比，不同之处为位于diskutil.c的init_disk_util函数中td_ioengine_flagged的返回值不同（正常为0，异常为非0）

init_disk_util函数与td_ioengine_flagged函数如下

```
void init_disk_util(struct thread_data *td)
{
	struct fio_file *f;
	unsigned int i;

	if (!td->o.do_disk_util ||
	    td_ioengine_flagged(td, FIO_DISKLESSIO | FIO_NODISKUTIL))
		return;

	for_each_file(td, f, i)
		f->du = __init_disk_util(td, f);
}


static inline bool td_ioengine_flagged(struct thread_data *td,
				       enum fio_ioengine_flags flags)
{
	return ((td->flags >> TD_ENG_FLAG_SHIFT) & flags) != 0;
}
```

总览代码，发现nfs.c中ioengine_ops结构体如下

```
struct ioengine_ops ioengine = {
	.name		= "nfs",
	.version	= FIO_IOOPS_VERSION,
	.setup		= fio_libnfs_setup,
	.queue		= fio_libnfs_queue,
	.getevents	= fio_libnfs_getevents,
	.event		= fio_libnfs_event,
	.cleanup	= fio_libnfs_cleanup,
	.open_file	= fio_libnfs_open,
	.close_file	= fio_libnfs_close,
	.commit     = fio_libnfs_commit,
	.flags      = FIO_DISKLESSIO | FIO_NOEXTEND | FIO_NODISKUTIL,
	.options	= options,
	.option_struct_size	= sizeof(struct fio_libnfs_options),
};
```

.flags      = FIO_DISKLESSIO | FIO_NOEXTEND | FIO_NODISKUTIL

而其他引擎中未有FIO_DISKLESSIO和FIO_NODISKUTIL二者同时出现的情况，由此导致不同。

尝试删除FIO_DISKLESSIO和FIO_NODISKUTIL后发现其他问题（代码运行进入错误分支）

据此可见FIO_DISKLESSIO和FIO_NODISKUTIL为fio为nfs引擎设置的不可缺少的特性

由此可见当前的fio3.30版本中不支持nfs引擎

