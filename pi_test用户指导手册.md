# pi_test使用手册

## 一、pi_test简介

​	pi_test程序是一个旨在训练的压力测试，用于使用Priority设置POSIX互斥对象的内核和C库代码路径，并继承属性(PTHREAD_PRIO_INHERIT)。

​	该程序由一个管理线程(主)，一个报告线程和一些数量为三组的线程称为“反转”组”。之所以这样称呼这些线程组，是因为它们会导致条件称为优先级反转，其中有高优先级线程阻塞，因为低优先级线程持有一个共享资源。没有违背逻辑的优先级反转是死锁条件。

​	每个反转组由三个线程组成:

1. 一个高优先级的线程
2. 中等优先级线程
3. 一个低优先级的线程

## 二、pi_test编译

##### 1.下载rt_tests源码（openharmony的musl中缺少pthread_attr_setaffinity_np函数，故采用Linux下静态链接的方式交叉编译）

```
git clone https://gitee.com/halley5/rt-tests-for-pi_test.git
```

##### 2.配置交叉编译工具链

（1）检查是否有交叉编译工具链

```
arm-none-linux-gnueabi-gcc -v
```

出现 *gcc version 4.3.3 (Sourcery G++ Lite 2009q1-203)*  即已经有交叉编译工具链，无需重新安装可直接进行第三步编译；反之进行下面的安装交叉编译工具链步骤。

（2）安装交叉编译工具链

```
wget http://www.codesourcery.com/sgpp/lite/arm/portal/package4571/public/arm-none-linux-gnueabi/arm-2009q1-203-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2 
```

​		 将文件拷贝至/usr/local文件夹，解压缩

```
cp arm-2009q1-203-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2 /usr/local
cd /usr/local
tar -jxvf arm-2009q1-203-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2
```

​	     配置环境变量

```
vi /etc/bash.bashrc
```

​		插入下面的语句；（复制粘贴上去就行）

```
export PATH=$PATH:/usr/local/arm-2009q1/bin/
```

​		激活配置

```
source /etc/bash.bashrc
```

​		再次执行 arm-none-linux-gnueabi-gcc -v ，查看编译工具链是否安装成功。


##### 3.编译

```
cd rt-tests
make
```

编译完成即可在当前目录下看到pi_stress可执行文件

## 三、pi_stress使用

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：pi_test的可执行文件放入（D:\test\pi_test) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将pi_test可执行文件发送到设备端

   ```
   hdc file send D:\test\pi_stress /data/test/
   ```

4. 在windows命令行上输入命令，进行板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./pi_stress --help
   ```

## 四、pi_test中的参数

| 参数             | 含义                                              |
| ---------------- | ------------------------------------------------- |
| --verbose        | 详细的大量输出                                    |
| --quiet          | 静音抑制运行输出                                  |
| --duration=<n>   | 测试运行的时间（默认无限长时间，单位为秒）        |
| --groups=<n>     | 设置反转组的数量（默认为3）                       |
| --inversions=<n> | 每组反演数（默认无限多）                          |
| --report=<path>  | 输出内容放置到指定路径下（默认为路径为/dev/null） |
| --rr             | 用SCHED_RR来测试线程（默认为SCHED_FIFO）          |
| --prompt         | 测试前提示                                        |
| --uniprocessor   | 强制所有线程在一个处理器上运行                    |
| --mlockall       | 锁定当前和未来内存                                |
| --debug          | 打开调试打印                                      |
| --version        | 在输出上打印版本号                                |
| --help           | 打印帮助信息                                      |

## 五、测试示例

**命令1：**

```
./pi_stress --verbose --duration=10 --groups=2
```

**输出结果：**

```
number of groups set to 2
Admin thread running on processor: 0
Test threads running on processor: 1
Creating 2 test groups
Starting PI Stress Test
Number of thread groups: 2
Duration of test run: 10 seconds
Number of inversions per group: unlimited
Test threads using scheduler policy: SCHED_FIFO
    Admin thread priority:  4
2 groups of 3 threads will be created
    High thread priority:   3
    Med  thread priority:   2
    Low thread priority:    1

Releasing all threads
Press Control-C to stop test
Current Inversions: 204730
duration reached (10 seconds)
setting shutdown flag
Stopping test
waiting for all threads to complete
All threads terminated!
Total inversion performed: 204733
Test Duration: 0 days, 0 hours, 0 minutes, 11 seconds
```

**命令2：**

```
./pi_stress --verbose  --duration=10 --groups=2 --rr --inversions=200000 --uniprocessor --mlockall --debug
```

**结果：**

```
number of groups set to 2
doing 200000 inversion per group
admin and test threads running on one processor
Creating 2 test groups
DEBUG: group 0 bound to cpu 0
DEBUG: creating low priority thread
DEBUG: creating medium priority thread
DEBUG: low_priority[0]: entering ready state
DEBUG: DEBUG: med_priority[0]: entering ready state
creating high priority thread
DEBUG: DEBUG: high_priority[0]: entering ready state
group 1 bound to cpu 1
DEBUG: creating low priority thread
DEBUG: creating medium priority thread
DEBUG: low_priority[1]: entering ready state
DEBUG: creating high priority thread
DEBUG: med_priority[1]: entering ready state
Starting PI Stress TestDEBUG:
high_priority[1]: entering ready state
Number of thread groups: 2
Duration of test run: 10 seconds
Number of inversions per group: 200000
Test threads using scheduler policy: SCHED_RR
    Admin thread priority:  4
2 groups of 3 threads will be created
    High thread priority:   3
    Med  thread priority:   2
    Low thread priority:    1

Releasing all threads
DEBUG: DEBUG: DEBUG: DEBUG: low_priority[0]: starting inversion loop
high_priority[0]: starting inversion loop
high_priority[1]: starting inversion loop
DEBUG: med_priority[0]: starting inversion loop
med_priority[1]: starting inversion loop
DEBUG: DEBUG: high_priority[0]: entering start state (0)
DEBUG: low_priority[1]: starting inversion loop
med_priority[0]: entering start state (0)
DEBUG: DEBUG: high_priority[1]: entering start state (0)
DEBUG: med_priority[1]: entering start state (0)
DEBUG: low_priority[0]: entering start wait (0)
low_priority[1]: entering start wait (0)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering running state
med_priority[0]: entering elevate state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering elevated wait
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: low_priority[0]: entering start wait (1)
high_priority[0]: entering start state (1)
DEBUG: high_priority[1]: unlocking mutex
DEBUG: med_priority[0]: entering start state (1)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: high_priority[1]: entering start state (1)
DEBUG: high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering start state (1)
DEBUG: low_priority[1]: entering start wait (1)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: low_priority[1]: mutex locked
DEBUG: med_priority[0]: entering finish state
low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (2)
DEBUG: high_priority[0]: entering start state (2)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (2)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[1]: entering start wait (2)
DEBUG: high_priority[1]: entering start state (2)
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering start state (2)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[1]: entering elevate state
low_priority[0]: entering elevated wait
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: got mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (3)
DEBUG: low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering start state (3)
DEBUG: DEBUG: high_priority[1]: entering start state (3)
DEBUG: DEBUG: med_priority[1]: entering start state (3)
low_priority[1]: entering start wait (3)
low_priority[0]: entering start wait (3)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: got mutex
DEBUG: DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: entering start state (4)
DEBUG: med_priority[0]: entering start state (4)
DEBUG: low_priority[0]: entering start wait (4)
low_priority[1]: entering finish wait
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (4)
DEBUG: high_priority[1]: entering start state (4)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (4)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (5)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (5)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering start wait (5)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
med_priority[1]: entering finish state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering start state (5)
DEBUG: med_priority[1]: entering start state (5)
DEBUG: low_priority[1]: entering start wait (5)
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: high_priority[1]: locking mutex
high_priority[0]: got mutex
DEBUG: DEBUG: DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
med_priority[1]: entering finish state
med_priority[0]: entering finish state
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: got mutex
DEBUG: med_priority[0]: entering start state (6)
DEBUG: high_priority[0]: entering start state (6)
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (6)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (6)
high_priority[1]: entering start state (6)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (6)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering start state (7)
DEBUG: med_priority[0]: entering start state (7)
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (7)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[1]: entering start state (7)
DEBUG: med_priority[1]: entering start state (7)
DEBUG: low_priority[1]: entering start wait (7)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: unlocking mutex
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (8)
DEBUG: DEBUG: med_priority[0]: entering start state (8)
DEBUG: reporter: starting report loop
Press Control-C to stop test
Current Inversions:
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: entering start wait (8)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering elevate state
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: entering running state
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (8)
DEBUG: low_priority[0]: mutex locked
DEBUG: med_priority[1]: entering start state (8)
DEBUG: low_priority[1]: entering start wait (8)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering locked wait
DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[1]: locking mutex
med_priority[0]: entering finish state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[0]: entering start state (9)
DEBUG: DEBUG: low_priority[0]: entering start wait (9)
med_priority[0]: entering start state (9)
med_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: DEBUG: high_priority[1]: entering start state (9)
DEBUG: med_priority[1]: entering start state (9)
med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering start wait (9)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (10)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (10)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (10)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering finish state
med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (10)
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering start state (10)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (10)
med_priority[0]: entering finish state
DEBUG: high_priority[1]: entering running state
DEBUG: low_priority[0]: entering finish wait
DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering start state (11)
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[1]: locking mutex
med_priority[0]: entering start state (11)
DEBUG: low_priority[0]: entering start wait (11)
low_priority[1]: entering elevated wait
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
med_priority[1]: entering finish state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[1]: entering start state (11)
DEBUG: high_priority[1]: entering start state (11)
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (11)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (12)
high_priority[0]: entering start state (12)
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (12)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (12)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (12)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (12)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (13)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (13)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering start wait (13)
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering locked wait
DEBUG: DEBUG: high_priority[1]: entering start state (13)
high_priority[0]: locking mutex
DEBUG: DEBUG: med_priority[1]: entering start state (13)
low_priority[1]: entering start wait (13)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: entering running state
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
med_priority[0]: entering start state (14)
DEBUG: high_priority[0]: entering start state (14)
DEBUG: DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (14)
med_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: low_priority[1]: entering finish wait
high_priority[0]: entering running state
DEBUG: high_priority[1]: entering finish state
DEBUG: DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (14)
low_priority[0]: mutex locked
med_priority[0]: entering elevate state
DEBUG: DEBUG: med_priority[1]: entering start state (14)
DEBUG: low_priority[1]: entering start wait (14)
low_priority[0]: entering locked wait
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering elevate state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: med_priority[1]: entering finish state
high_priority[0]: entering start state (15)
DEBUG: med_priority[0]: entering start state (15)
DEBUG: low_priority[0]: entering start wait (15)
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[0]: entering elevate state
high_priority[0]: entering running state
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: DEBUG: high_priority[1]: entering start state (15)
low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
med_priority[1]: entering start state (15)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (15)
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering start state (16)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_prihigh_priority[1]: got mutex
Current Inversions: 31
DEBUG: high_priority[1]: unlocking mutex
med_priority[0]: entering start state (16)
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (16)
DEBUG: high_priority[1]: entering start state (16)
DEBUG: DEBUG: low_priority[1]: entering start wait (16)
high_priority[0]: entering running state
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: med_priority[1]: entering start state (16)
low_priority[0]: claiming mutex
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[0]: entering finish state
med_priority[1]: entering finish state
low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[0]: entering start state (17)
DEBUG: high_priority[1]: unlocking mutex
DEBUG: med_priority[0]: entering start state (17)
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (17)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (17)
high_priority[1]: entering start state (17)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (17)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering start state (18)
DEBUG: med_priority[0]: entering start state (18)
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (18)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering start state (18)
DEBUG: med_priority[1]: entering start state (18)
DEBUG: low_priority[1]: entering start wait (18)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
low_priority[0]: entering finish wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering start state (19)
DEBUG: high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering start state (19)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (19)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering finish state
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: entering running state
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (19)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (19)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
low_priority[1]: entering start wait (19)
low_priority[0]: entering finish wait
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering start state (20)
DEBUG: med_priority[0]: entering start state (20)
DEBUG: med_priority[1]: entering elevate state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (20)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (20)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (20)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (20)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering start state (21)
DEBUG: med_priority[0]: entering start state (21)
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (21)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[0]: entering elevated wait
high_priority[0]: locking mutex
high_priority[1]: got mutex
DEBUG: DEBUG: DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: entering start state (21)
DEBUG: DEBUG: low_priority[1]: entering start wait (21)
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[1]: entering start state (21)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (22)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (22)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (22)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: DEBUG: high_priority[1]: entering start state (22)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (22)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (22)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (23)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (23)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
low_priority[0]: entering start wait (23)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (23)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (23)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (23)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (24)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (24)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
Current Inversions: 47]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering start wait (24)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: entering start state (24)
DEBUG: med_priority[1]: entering start state (24)
DEBUG: low_priority[1]: entering start wait (24)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
high_priority[0]: entering start state (25)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (25)
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (25)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (25)
high_priority[1]: entering start state (25)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (25)
low_priority[0]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering elevate state
low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[0]: entering finish wait
high_priority[0]: entering finish state
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (26)
DEBUG: med_priority[0]: entering start state (26)
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (26)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (26)
high_priority[1]: entering start state (26)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (26)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: entering start state (27)
DEBUG: med_priority[0]: entering start state (27)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (27)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: locking mutex
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: entering locked wait
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: DEBUG: low_priority[1]: entering finish wait
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[1]: entering start state (27)
DEBUG: med_priority[1]: entering start state (27)
DEBUG: low_priority[1]: entering start wait (27)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
med_priority[0]: entering finish state
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: locking mutex
high_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: med_priority[1]: entering finish state
high_priority[0]: entering start state (28)
DEBUG: med_priority[0]: entering start state (28)
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (28)
DEBUG: DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: high_priority[1]: entering start state (28)
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering start state (28)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering start wait (28)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
high_priority[1]: entering running state
DEBUG: DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
high_priority[0]: entering start state (29)
DEBUG: DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
med_priority[0]: entering start state (29)
DEBUG: DEBUG: high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (29)
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: entering running state
DEBUG: DEBUG: high_priority[1]: entering start state (29)
DEBUG: DEBUG: low_priority[1]: entering start wait (29)
med_priority[1]: entering start state (29)
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: entering running state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[1]: locking mutex
low_priority[0]: entering elevated wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[1]: entering finish wait
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: got mutex
DEBUG: DEBUG: high_priority[1]: entering finish state
high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (30)
DEBUG: high_priority[0]: entering start state (30)
DEBUG: med_priority[1]: entering start state (30)
DEBUG: med_priority[0]: entering start state (30)
DEBUG: low_priority[0]: entering start wait (30)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering start wait (30)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering start state (31)
DEBUG: med_priority[1]: entering start state (31)
DEBUG: low_priority[1]: entering start wait (31)
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering running state
DEBUG: DEBUG: high_priority[0]: entering start state (31)
DEBUG: DEBUG: low_priority[0]: entering start wait (31)
med_priority[0]: entering start state (31)
med_priority[1]: entering elevate state
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: DEBUG: med_priority[0]: entering elevate state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: mutex locked
DEBUG: DEBUG: low_priority[0]: entering elevated wait
low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (32)
DEBUG: med_priority[0]: entering start state (32)
med_priority[1]: entering finish state
DEBUG: Current Inversions: 63ing start wait (32)
high_priority[0]: entering running state
DEBUG: DEBUG: low_priority[1]: entering finish wait
med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: entering start state (32)
DEBUG: med_priority[1]: entering start state (32)
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering start wait (32)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
med_priority[0]: entering start state (33)
DEBUG: high_priority[0]: entering start state (33)
DEBUG: DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (33)
med_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (33)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (33)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (33)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (34)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (34)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (34)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (34)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (34)
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (34)
med_priority[0]: entering finish state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: entering start state (35)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
med_priority[0]: entering start state (35)
DEBUG: DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[0]: entering start wait (35)
low_priority[1]: entering elevated wait
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
low_priority[0]: mutex locked
med_priority[1]: entering finish state
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
high_priority[1]: entering start state (35)
DEBUG: DEBUG: med_priority[1]: entering start state (35)
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (35)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (36)
high_priority[0]: entering start state (36)
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (36)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (36)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (36)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering start wait (36)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (37)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering start state (37)
DEBUG: low_priority[0]: entering start wait (37)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[1]: entering start state (37)
DEBUG: med_priority[1]: entering start state (37)
DEBUG: low_priority[1]: entering start wait (37)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
med_priority[0]: entering finish state
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: low_priority[0]: entering start wait (38)
DEBUG: high_priority[0]: entering start state (38)
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (38)
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: entering running state
DEBUG: DEBUG: high_priority[1]: entering start state (38)
DEBUG: DEBUG: low_priority[1]: entering start wait (38)
med_priority[1]: entering start state (38)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: DEBUG: high_priority[0]: entering start state (39)
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (39)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (39)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering finish state
med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (39)
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering start state (39)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (39)
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering finish wait
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (40)
DEBUG: low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering start state (40)
DEBUG: DEBUG: high_priority[1]: entering start state (40)
Current Inversions: 80rity[1]: entering start state (40)
low_priority[0]: entering start wait (40)
low_priority[1]: entering start wait (40)
DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: got mutex
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (41)
DEBUG: DEBUG: high_priority[0]: entering start state (41)
DEBUG: DEBUG: low_priority[0]: entering start wait (41)
med_priority[0]: entering start state (41)
med_priority[1]: entering start state (41)
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: low_priority[1]: entering start wait (41)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[1]: entering finish wait
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (42)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering start state (42)
DEBUG: med_priority[0]: entering start state (42)
med_priority[1]: entering start state (42)
DEBUG: low_priority[1]: entering start wait (42)
low_priority[0]: entering start wait (42)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[0]: entering finish wait
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: entering start state (43)
DEBUG: high_priority[1]: entering start state (43)
DEBUG: med_priority[1]: entering start state (43)
DEBUG: med_priority[0]: entering start state (43)
DEBUG: low_priority[0]: entering start wait (43)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering start wait (43)
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: DEBUG: high_priority[1]: entering start state (44)
DEBUG: med_priority[1]: entering start state (44)
DEBUG: low_priority[1]: entering start wait (44)
low_priority[0]: entering finish wait
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[1]: locking mutex
high_priority[0]: entering start state (44)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
med_priority[0]: entering start state (44)
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (44)
DEBUG: high_priority[0]: entering running state
DEBUG: low_priority[1]: entering finish wait
med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: high_priority[1]: entering start state (45)
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering start state (45)
DEBUG: low_priority[1]: entering start wait (45)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering elevate state
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
med_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
low_priority[0]: entering finish wait
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (45)
DEBUG: med_priority[0]: entering start state (45)
DEBUG: DEBUG: low_priority[0]: entering start wait (45)
med_priority[1]: entering finish state
DEBUG: high_priority[0]: entering running state
low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[1]: entering start state (46)
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering start state (46)
DEBUG: low_priority[1]: entering start wait (46)
low_priority[0]: entering elevated wait
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
med_priority[0]: entering finish state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: entering start state (46)
DEBUG: med_priority[0]: entering start state (46)
med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (46)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering finish wait
med_priority[0]: entering elevate state
DEBUG: DEBUG: DEBUG: high_priority[1]: entering start state (47)
DEBUG: med_priority[1]: entering start state (47)
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering start wait (47)
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: high_priority[0]: entering start state (47)
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (47)
DEBUG: low_priority[0]: entering start wait (47)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (48)
DEBUG: DEBUG: med_priority[1]: entering start state (48)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering start wait (48)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
high_priority[1]: entering running state
DEBUG: DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: entering locked wait
Current Inversions: 95
DEBUG: high_priority[1]: locking mutex
low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[0]: entering start state (48)
DEBUG: DEBUG: low_priority[0]: entering start wait (48)
med_priority[0]: entering start state (48)
med_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: got mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: entering start state (49)
DEBUG: med_priority[1]: entering start state (49)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (49)
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering start state (49)
DEBUG: med_priority[0]: entering start state (49)
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (49)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: high_priority[1]: entering start state (50)
high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering start state (50)
DEBUG: low_priority[1]: entering start wait (50)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
DEBUG: DEBUG: DEBUG: high_priority[0]: entering start state (50)
DEBUG: med_priority[0]: entering start state (50)
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (50)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[0]: entering elevated wait
high_priority[0]: locking mutex
high_priority[1]: got mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: DEBUG: high_priority[1]: entering start state (51)
DEBUG: DEBUG: high_priority[0]: entering start state (51)
DEBUG: med_priority[0]: entering start state (51)
med_priority[1]: entering start state (51)
DEBUG: low_priority[1]: entering start wait (51)
low_priority[0]: entering start wait (51)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: med_priority[1]: entering finish state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: high_priority[0]: entering start state (52)
DEBUG: med_priority[0]: entering start state (52)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (52)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (52)
high_priority[1]: entering start state (52)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering start wait (52)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (53)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (53)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (53)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering finish state
med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (53)
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering start state (53)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (53)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
DEBUG: DEBUG: low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
low_priority[1]: claiming mutex
DEBUG: DEBUG: DEBUG: high_priority[0]: entering start state (54)
low_priority[1]: mutex locked
DEBUG: med_priority[0]: entering start state (54)
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (54)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: entering start state (55)
DEBUG: med_priority[0]: entering start state (55)
DEBUG: high_priority[1]: entering start state (54)
DEBUG: med_priority[1]: entering start state (54)
DEBUG: low_priority[1]: entering start wait (54)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering start wait (55)
med_priority[1]: entering elevate state
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (55)
DEBUG: high_priority[0]: entering start state (56)
DEBUG: med_priority[1]: entering start state (55)
DEBUG: med_priority[0]: entering start state (56)
DEBUG: low_priority[0]: entering start wait (56)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering start wait (55)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering elevated wait
Current Inversions: 111 unlocking mutex
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (56)
DEBUG: DEBUG: low_priority[1]: entering start wait (56)
high_priority[0]: entering start state (57)
DEBUG: med_priority[0]: entering start state (57)
DEBUG: med_priority[1]: entering start state (56)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering start wait (57)
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[1]: locking mutex
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: high_priority[1]: entering start state (57)
high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering start state (57)
DEBUG: low_priority[1]: entering start wait (57)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering start state (58)
DEBUG: med_priority[0]: entering start state (58)
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (58)
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering start state (58)
DEBUG: med_priority[1]: entering start state (58)
DEBUG: low_priority[1]: entering start wait (58)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (59)
high_priority[0]: entering start state (59)
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (59)
DEBUG: DEBUG: med_priority[1]: entering start state (59)
med_priority[0]: entering elevate state
DEBUG: DEBUG: low_priority[1]: entering start wait (59)
low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering start state (59)
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[1]: entering finish state
med_priority[0]: entering finish state
low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering finish wait
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (60)
DEBUG: high_priority[1]: entering start state (60)
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (60)
DEBUG: high_priority[0]: entering start state (60)
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: med_priority[0]: entering start state (60)
DEBUG: low_priority[0]: entering start wait (60)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (61)
high_priority[1]: entering start state (61)
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (61)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: entering start state (61)
DEBUG: med_priority[0]: entering start state (61)
DEBUG: low_priority[0]: entering start wait (61)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
low_priority[0]: unlocking mutex
low_priority[1]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: entering finish state
med_priority[1]: entering finish state
low_priority[1]: unlocking mutex
DEBUG: high_priority[0]: entering start state (62)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (62)
DEBUG: low_priority[0]: entering start wait (62)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering finish wait
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: entering start state (62)
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering start state (62)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering start wait (62)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (63)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (63)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (63)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering finish state
med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[1]: entering start state (63)
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering start state (63)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (63)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: entering start state (64)
low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (64)
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering start wait (64)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: entering start state (64)
low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering start state (64)
Current Inversions: 128: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering start wait (64)
DEBUG: DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: DEBUG: low_priority[0]: entering finish wait
low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering start state (65)
DEBUG: med_priority[0]: entering start state (65)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering start wait (65)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering finish wait
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: low_priority[1]: entering start wait (65)
med_priority[0]: entering finish state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: entering start state (65)
DEBUG: med_priority[1]: entering start state (65)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering start state (66)
DEBUG: high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering start state (66)
DEBUG: low_priority[0]: entering start wait (66)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering finish state
DEBUG: DEBUG: high_priority[1]: entering start state (66)
DEBUG: DEBUG: med_priority[1]: entering start state (66)
low_priority[0]: entering finish wait
low_priority[1]: entering start wait (66)
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering start state (67)
DEBUG: med_priority[0]: entering start state (67)
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (67)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: entering start state (67)
low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering start state (67)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[1]: entering start wait (67)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering finish state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: high_priority[1]: locking mutex
high_priority[0]: entering finish state
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (68)
DEBUG: high_priority[0]: entering start state (68)
high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (68)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering start state (68)
med_priority[1]: entering start state (68)
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (68)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: entering start state (69)
DEBUG: med_priority[0]: entering start state (69)
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (69)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: med_priority[1]: entering finish state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering finish wait
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: entering start state (69)
DEBUG: med_priority[1]: entering start state (69)
DEBUG: med_priority[0]: entering finish state
low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (69)
DEBUG: DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[1]: entering running state
high_priority[0]: entering start state (70)
DEBUG: med_priority[0]: entering start state (70)
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (70)
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: low_priority[0]: entering locked wait
high_priority[1]: unlocking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering finish state
med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: entering start state (70)
med_priority[0]: entering finish state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering start state (70)
DEBUG: low_priority[1]: entering start wait (70)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering start state (71)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (71)
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering start wait (71)
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: high_priority[1]: entering start state (71)
DEBUG: DEBUG: low_priority[1]: entering start wait (71)
med_priority[1]: entering start state (71)
med_priority[0]: entering finish state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering finish wait
DEBUG: med_priority[1]: entering finish state
DEBUG: DEBUG: high_priority[0]: entering start state (72)
DEBUG: DEBUG: med_priority[0]: entering start state (72)
low_priority[1]: entering finish wait
low_priority[0]: entering start wait (72)
DEBUG: DEBUG: low_priority[1]: entering start wait (72)
DEBUG: high_priority[1]: entering start state (72)
high_priority[0]: entering running state
DEBUG: med_priority[1]: entering start state (72)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
med_priority[0]: entering elevate state
med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: Current Inversions: 144ng locked wait
high_priority[0]: locking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: got mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (73)
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (73)
DEBUG: low_priority[0]: entering start wait (73)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering finish wait
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: entering start state (73)
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
med_priority[1]: entering start state (73)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering start wait (73)
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (74)
DEBUG: DEBUG: med_priority[0]: entering start state (74)
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
low_priority[0]: entering start wait (74)
DEBUG: high_priority[0]: entering running state
DEBUG: low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering start state (74)
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
med_priority[1]: entering start state (74)
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering start wait (74)
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering start state (75)
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (75)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: entering start wait (75)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering finish state
med_priority[0]: entering elevate state
DEBUG: DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: entering start state (75)
DEBUG: med_priority[1]: entering start state (75)
DEBUG: low_priority[1]: entering start wait (75)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering elevated wait
DEBUG: high_priority[0]: entering start state (76)
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (76)
DEBUG: low_priority[0]: entering start wait (76)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (76)
DEBUG: DEBUG: med_priority[1]: entering start state (76)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering start wait (76)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
high_priority[1]: entering running state
DEBUG: DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
high_priority[0]: entering start state (77)
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[0]: entering start state (77)
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (77)
DEBUG: DEBUG: DEBUG: med_priority[1]: entering start state (77)
high_priority[1]: entering start state (77)
high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: entering start wait (77)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: med_priority[1]: entering elevate state
high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[0]: entering start state (78)
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: med_priority[0]: entering start state (78)
DEBUG: DEBUG: low_priority[0]: entering start wait (78)
high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
high_priority[1]: got mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: unlocking mutex
DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (79)
DEBUG: low_priority[1]: entering finish wait
DEBUG: med_priority[0]: entering start state (79)
DEBUG: DEBUG: high_priority[1]: entering start state (78)
DEBUG: DEBUG: med_priority[1]: entering start state (78)
low_priority[0]: entering start wait (79)
low_priority[1]: entering start wait (78)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering elevated wait
DEBUG: high_priority[0]: locking mutex
high_priority[1]: locking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (79)
DEBUG: high_priority[0]: entering start state (80)
DEBUG: med_priority[1]: entering start state (79)
DEBUG: med_priority[0]: entering start state (80)
DEBUG: low_priority[0]: entering start wait (80)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering start wait (79)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: DEBUG: low_priority[1]: entering finish wait
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering finish state
DEBUG: low_priority[0]: entering finish wait
Current Inversions: 161: entering start state (80)
DEBUG: high_priority[0]: entering start state (81)
DEBUG: med_priority[0]: entering start state (81)
DEBUG: med_priority[1]: entering start state (80)
DEBUG: low_priority[1]: entering start wait (80)
DEBUG: low_priority[0]: entering start wait (81)
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[1]: locking mutex
med_priority[0]: entering finish state
low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: DEBUG: high_priority[0]: unlocking mutex
DEBUG: med_priority[1]: entering finish state
high_priority[0]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (81)
DEBUG: high_priority[0]: entering start state (82)
DEBUG: med_priority[0]: entering start state (82)
DEBUG: low_priority[0]: entering start wait (82)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
med_priority[1]: entering start state (81)
med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering start wait (81)
DEBUG: DEBUG: DEBUG: high_priority[1]: entering running state
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering elevated wait
DEBUG: DEBUG: med_priority[1]: entering finish state
high_priority[0]: entering start state (83)
DEBUG: med_priority[0]: entering start state (83)
DEBUG: low_priority[0]: entering start wait (83)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: unlocking mutex
med_priority[0]: entering elevate state
DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (82)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (82)
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: entering start wait (82)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
DEBUG: high_priority[0]: entering finish state
med_priority[0]: entering finish state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[0]: entering finish wait
low_priority[1]: entering locked wait
DEBUG: DEBUG: high_priority[0]: entering start state (84)
high_priority[1]: locking mutex
DEBUG: DEBUG: med_priority[0]: entering start state (84)
DEBUG: low_priority[1]: entering elevated wait
low_priority[0]: entering start wait (84)
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[1]: entering finish state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: DEBUG: DEBUG: high_priority[0]: locking mutex
low_priority[1]: unlocking mutex
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: entering start state (83)
DEBUG: high_priority[0]: entering start state (85)
DEBUG: med_priority[1]: entering start state (83)
DEBUG: med_priority[0]: entering start state (85)
DEBUG: low_priority[0]: entering start wait (85)
DEBUG: DEBUG: DEBUG: high_priority[0]: entering running state
low_priority[1]: entering start wait (83)
med_priority[0]: entering elevate state
DEBUG: DEBUG: high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: got mutex
DEBUG: DEBUG: med_priority[0]: entering finish state
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering start state (84)
DEBUG: med_priority[1]: entering start state (84)
DEBUG: low_priority[1]: entering start wait (84)
low_priority[0]: entering finish wait
DEBUG: DEBUG: DEBUG: med_priority[0]: entering start state (86)
DEBUG: high_priority[0]: entering start state (86)
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering start wait (86)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (85)
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
med_priority[1]: entering start state (85)
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: low_priority[1]: entering start wait (85)
DEBUG: DEBUG: med_priority[0]: entering start state (87)
DEBUG: DEBUG: low_priority[1]: claiming mutex
low_priority[0]: entering start wait (87)
DEBUG: high_priority[0]: entering start state (87)
DEBUG: high_priority[1]: entering running state
DEBUG: DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
med_priority[1]: entering elevate state
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: high_priority[1]: locking mutex
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
low_priority[0]: entering elevated wait
DEBUG: DEBUG: DEBUG: high_priority[1]: entering start state (86)
DEBUG: med_priority[0]: entering finish state
med_priority[1]: entering start state (86)
low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: entering start wait (86)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: DEBUG: high_priority[1]: entering running state
high_priority[0]: got mutex
DEBUG: DEBUG: DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
low_priority[0]: entering finish wait
med_priority[1]: entering elevate state
DEBUG: high_priority[0]: entering start state (88)
low_priority[1]: mutex locked
DEBUG: DEBUG: med_priority[0]: entering start state (88)
low_priority[1]: entering locked wait
DEBUG: DEBUG: low_priority[0]: entering start wait (88)
high_priority[1]: locking mutex
DEBUG: high_priority[0]: entering running state
DEBUG: med_priority[0]: entering elevate state
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
low_priority[1]: entering finish wait
DEBUG: high_priority[0]: locking mutex
DEBUG: DEBUG: high_priority[1]: entering start state (87)
DEBUG: DEBUG: med_priority[1]: entering start state (87)
low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
low_priority[1]: entering start wait (87)
DEBUG: DEBUG: DEBUG: med_priority[0]: entering finish state
high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
high_priority[1]: entering running state
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[1]: claiming mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: low_priority[1]: entering locked wait
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[1]: locking mutex
DEBUG: DEBUG: high_priority[0]: entering start state (89)
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
med_priority[0]: entering start state (89)
DEBUG: DEBUG: low_priority[0]: entering start wait (89)
high_priority[1]: got mutex
DEBUG: low_priority[0]: claiming mutex
DEBUG: high_priority[0]: entering running state
DEBUG: high_priority[1]: unlocking mutex
Current Inversions: 176: entering finish state
duration reached (10 seconds)
setting shutdown flag
DEBUG: reporter: finished
Stopping test
waiting for all threads to complete
DEBUG: med_priority[0]: entering elevate state
DEBUG: med_priority[1]: entering finish state
DEBUG: low_priority[1]: entering finish wait
DEBUG: low_priority[0]: mutex locked
DEBUG: DEBUG: high_priority[1]: entering start state (88)
DEBUG: DEBUG: med_priority[1]: entering start state (88)
low_priority[0]: entering locked wait
low_priority[1]: entering start wait (88)
DEBUG: DEBUG: low_priority[1]: claiming mutex
DEBUG: high_priority[1]: entering running state
high_priority[0]: locking mutex
DEBUG: med_priority[1]: entering elevate state
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: low_priority[1]: mutex locked
DEBUG: DEBUG: DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
low_priority[1]: entering locked wait
DEBUG: high_priority[0]: entering finish state
DEBUG: high_priority[1]: locking mutex
med_priority[0]: entering finish state
DEBUG: low_priority[1]: entering elevated wait
DEBUG: low_priority[1]: unlocking mutex
DEBUG: low_priority[0]: entering finish wait
DEBUG: DEBUG: high_priority[1]: got mutex
DEBUG: high_priority[1]: unlocking mutex
DEBUG: high_priority[1]: entering finish state
DEBUG: high_priority[0]: entering start state (90)
DEBUG: med_priority[0]: entering start state (90)
DEBUG: med_priority[1]: entering finish state
low_priority[1]: entering finish wait
DEBUG: low_priority[0]: entering start wait (90)
DEBUG: DEBUG: med_priority[0]: entering elevate state
DEBUG: high_priority[0]: entering running state
high_priority[1]: entering done barrier
DEBUG: med_priority[1]: entering done barrier
DEBUG: low_priority[1]: entering done barrier
DEBUG: low_priority[0]: claiming mutex
DEBUG: low_priority[0]: mutex locked
DEBUG: low_priority[0]: entering locked wait
DEBUG: high_priority[0]: locking mutex
DEBUG: low_priority[0]: entering elevated wait
DEBUG: low_priority[0]: unlocking mutex
DEBUG: high_priority[0]: got mutex
DEBUG: high_priority[0]: unlocking mutex
DEBUG: high_priority[0]: entering finish state
DEBUG: med_priority[0]: entering finish state
DEBUG: low_priority[0]: entering finish wait
DEBUG: high_priority[0]: entering done barrier
DEBUG: med_priority[0]: entering done barrier
DEBUG: low_priority[0]: entering done barrier
DEBUG: All threads terminated!
high_priority[0]: exiting
DEBUG: med_priority[0]: exiting
Total inversion performed: 180
Test Duration: 0 days, 0 hours, 0 minutes, 11 seconds
DEBUG: low_priority[0]: exiting
```
