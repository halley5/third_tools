

# 通过新建子系统并在该子系统的部件下添加模块的方式添加三方工具

## 一、使用方法

1.进入openharmony的third_party目录

2.clone本仓和相关其他仓

```
cd third_party
git clone https://gitee.com/halley5/third_tools.git
cd third_tools
git clone https://gitee.com/halley5/ltp.git
git clone https://gitee.com/halley5/stress-ng.git
git clone https://gitee.com/halley5/memstress.git
git clone https://gitee.com/halley5/memtester.git
git clone https://gitee.com/halley5/libaio_0.3.113.git
git clone https://gitee.com/halley5/rt-test.git
git clone https://gitee.com/halley5/fio.git
```

3.进行外部配置改动

4.进行编译

## 二、外部配置改动

1.vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

#包围的为新增部分

```
{
  "product_name": "hispark_taurus_standard",
  "device_company": "hisilicon",
  "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
  "target_cpu": "arm",
  "type": "standard",
  "version": "3.0",
  "board": "hispark_taurus",
  "enable_ramdisk": true,
  "subsystems": [
#############################################
    {
      "subsystem": "third_tools",
      "components": [
        {
          "component": "tools",
          "features": []
        }
      ]
    },
#############################################
    {
      "subsystem": "arkui",
      "components": [
        {
          "component": "ace_engine_standard",
          "features": []
        },
        {
          "component": "napi",
          "features": []
        }
      ]
    },
```

2.vendor/hihope/rk3568/目录config.json文件添加对应的部件。

#包围的为新增部分

```
{
  "product_name": "rk3568",
  "device_company": "rockchip",
  "device_build_path": "device/board/hihope/rk3568",
  "target_cpu": "arm",
  "type": "standard",
  "version": "3.0",
  "board": "rk3568",
  "enable_ramdisk": true,
  "build_selinux": true,
  "subsystems": [
########################################
    {
      "subsystem": "third_tools",
      "components": [
        {
          "component": "tools",
          "features": []
        }
      ]
    },
########################################
    {
      "subsystem": "arkui",
      "components": [
        {
          "component": "ace_engine_standard",
          "features": []
        },
        {
          "component": "napi",
          "features": []
        }
      ]
    },
```

3.build目录下的subsystem_config.json文件添加新建的子系统的配置。

#包围的为新增部分

```
{
  "ace": {
    "path": "foundation/ace",
    "name": "ace"
  },
############################################
  "third_tools": {
    "path": "third_party/third_tools",
    "name": "third_tools"
  },
############################################
  "ai": {
    "path": "foundation/ai",
    "name": "ai"
  },
```

4.修改third_party/musl/include/endian.h文件使编译通过

#包围的为新增部分

```
#ifndef _ENDIAN_H
#define _ENDIAN_H

####################################################################
#pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
#pragma clang diagnostic ignored "-Wshift-op-parentheses"
####################################################################

#include <features.h>
```

## 三、编译命令

使用hb编译：

HI3516DV300：

```
hb set
选择hispark_taurus_standard
hb build -T third_party/third_tools/memtester:memtester
hb build -T third_party/third_tools/ltp:fsstress
hb build -T third_party/third_tools/memstress:memstress
hb build -T third_party/third_tools/ltp:aio
hb build -T third_party/third_tools/rt-test:cyclictest
hb build -T third_party/third_tools/fio:fio
hb build -T third_party/third_tools/stress-ng:stress-ng
```

RK3568:

```
hb set
选择rk3568
hb build -T third_party/third_tools/memtester:memtester
hb build -T third_party/third_tools/ltp:fsstress
hb build -T third_party/third_tools/memstress:memstress
hb build -T third_party/third_tools/ltp:aio
hb build -T third_party/third_tools/rt-test:cyclictest
hb build -T third_party/third_tools/fio:fio
hb build -T third_party/third_tools/stress-ng:stress-ng
```

## 四、结果

可在```out/hispark_taurus/```和```out/rk3568/```下看到编译成功的memtester、fsstress、msmstress、aio、cyclictest

## 五、相关仓

[ltp仓](https://gitee.com/halley5/ltp.git):包含fsstress和aio-stress

[memstress仓](https://gitee.com/halley5/memstress.git):包含memstress

[memtester仓](https://gitee.com/halley5/memtester.git):包含memtester

[rt-test仓](https://gitee.com/halley5/rt-test.git):包含cyclictest

[rt-tests for pi_test仓](https://gitee.com/halley5/rt-tests-for-pi_test):包含pi_test

[fio仓](https://gitee.com/halley5/fio.git):包含fio

## 六、相关仓内容说明

详见：

[fsstress内容说明](https://gitee.com/halley5/third_tools/tree/master/fsstress内容说明.md)

[memstress内容说明](https://gitee.com/halley5/third_tools/tree/master/memstress内容说明.md)

[memtester内容说明](https://gitee.com/halley5/third_tools/tree/master/memtester内容说明.md)

[aio-stress内容说明](https://gitee.com/halley5/third_tools/tree/master/aio-stress内容说明.md)

[cyclictest内容说明](https://gitee.com/halley5/third_tools/tree/master/cyclictest内容说明.md)

[pi_test内容说明](https://gitee.com/halley5/third_tools/blob/master/pi_test%E5%86%85%E5%AE%B9%E8%AF%B4%E6%98%8E.md)
