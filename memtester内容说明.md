# memtester内容说明

## 一、介绍

memtester相关内容位于https://gitee.com/halley5/ltp.git

基于[memtester-4.5.1.tar.gz](https://pyropus.ca./software/memtester/old-versions/memtester-4.5.1.tar.gz)

## 二、主要修改

主要增加了一个BUILD.gn文件

BUILD.gn内容：

```
import("//build/ohos.gni")

executable("memtester"){
    sources =[
        "memtester.c",
        "tests.c"
    ]

    include_dirs = [
        ".",
        "//third_party/musl/include",
        "//third_party/musl/include/sys"
    ]

    deps = [
        "//third_party/musl:musl_all"
    ]
}
```

对比视图：https://gitee.com/halley5/memtester/commit/4579b0c42b565dc594a9afa2bb19fc5514f2e3ee
