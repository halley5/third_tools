# memstress内容说明

## 一、介绍

fsstress相关内容位于https://gitee.com/halley5/memstress.git

基于https://github.com/yoffy/memstress.git

## 二、主要修改

主要增加了一个BUILD.gn文件

BUILD.gn内容：

```
import("//build/test.gni")

executable("memstress"){
    sources =[
        "memstress.cpp",
    ]

    include_dirs = [
    ]

    deps = [
        "//third_party/musl:musl_all",
    ]
}
```

对比视图：https://gitee.com/halley5/memstress/commit/8dc7feb09e0215e457f315bf8c13274beff5e373

