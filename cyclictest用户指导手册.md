# cyclictest使用文档

## 一、cyclictest简介

​	cyclictest是rt-tests下一个使用最广泛的测试工具，主要用来测试使用内核的延迟，从而判断内核的实时性。

## 二、cyclictest安装

1. 进入openharmony的third_party目录,clone本仓和cyclictest的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/rt-test.git
   ```

2. 进行外部配置改动

   vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "hispark_taurus_standard",
     "device_company": "hisilicon",
     "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "hispark_taurus",
     "enable_ramdisk": true,
     "subsystems": [
   #############################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   #############################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   vendor/hihope/rk3568/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "device_company": "rockchip",
     "device_build_path": "device/board/hihope/rk3568",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "rk3568",
     "enable_ramdisk": true,
     "build_selinux": true,
     "subsystems": [
   ########################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   ########################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的cyclictest。

## 三、编译命令

### 3.1基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/cyclictest/cyclictest_master:cyclictest
```

### 3.2基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/cyclictest/cyclictest_master:cyclictest
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：cyclictest的可执行文件放入（D:\test\cyclictest) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data/test
   ```

3. 在windows命令行上输入命令，从host端将pi_test可执行文件发送到设备端

   ```
   hdc file send D:\test\cyclictest /data/test/
   ```

4. 在windows命令行上输入命令，进行板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd dev/
   mkdir shm/
   chmod 777 shm  (因为板内环境不包含shm共享文件夹，如不创建，测试中会产生报错)
   cd data/test/
   ./cyclictest
   ```

## 四、cyclictest中的参数

| 参数                                                         | 含义                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| -a [NUM]<br/>–affinity                                       | 在处理器N上运行线程N，如果可能，使用NUM引脚处理NUM的所有线程 |
| -A USEC --aligned=USEC                                       | 将线程唤醒与特定偏移量对齐                                   |
| -b USE<br/>–breaktrace=USEC                                  | 调试选项，用于控制实施抢占补丁中的延迟跟踪器。当延时大于USEC指定的值时，发送停止跟踪。USEC,单位为μ \muμs |
| -c CLOCK<br/>--clock=CLOCK                                   | 选择时钟<br/>0 = CLOCK_MONOTONIC 单调递增系统(默认) 1 = CLOCK_REALTIME一天中的时间 |
| -C<br/>–context                                              | 上下文切换追踪（与-b一起使用）                               |
| -d DIST<br/>–distance=DIST                                   | 线程间隔的距离，默认值为500                                  |
| -D --duration=TIME                                           | 指定测试运行的长度。                                         |
| -E<br/>event                                                 | 事件追踪（与-b一起使用）                                     |
| -F --fifo=<path>                                             | 在 path 处创建一个命名管道并将统计信息写入其中               |
| -f<br/>–ftrace                                               | ftrace函数跟踪（通常与-b 配套使用，其实通常使用 -b 即可，不使用 -f ） |
| -h HISTNUM<br/>–histogram=US                                 | 在执行完后在标准输出设备上画出延迟的直方图（很多线程有相同的权限）US为最大的跟踪时间限制，这个在下面介绍实例时可以用到，结合gnuplot 可以画出我们测试的结果图。 |
| -H --histofall=US                                            | 与 -h 相同，只是多了一个汇总列                               |
| -i INTV<br/>–interval=INTV    --json=FILENAME --laptop       | <线程的基本间隔，默认为1000（单位为μ \muμs）<br/>将最终结果写入 FILENAME，JSON 格式  <br /> 运行循环测试时节省电池<br/>这会给你带来更差的实时结果<br/>但不会这么快耗尽你的电池 |
| -I<br/>–irqsoff                                              | Irfsoff tracing（与-b一起使用）                              |
| -l LOOPS<br/>–loops=LOOPS                                    | 循环的个数，默认为0（无穷个）                                |
| -m<br/>--mlockall                                            | 锁定当前和将来的内存分配                                     |
| -M --refresh_on_max                                          | 延迟更新屏幕直到一个新的最大值<br/>  延迟被击中。适用于低带宽。 |
| -N --nsecs                                                   | 在 ns 而不是 us 中打印结果（默认 us）                        |
| -n<br/>–nanosleep                                            | 使用 clock_nanosleep                                         |
| -o RED --oscope=RED                                          | 示波器模式，减少 RED 的详细输出                              |
| -p PRIO<br/>–prio=PRIO                                       | 最高优先级线程的优先级，使用方法： -p 90/–prio=90            |
| -q<br/>–quiet                                                | 使用-q 参数运行时不打印信息，只在退出时打印概要内容，结合-h HISTNUM参数会在退出时打印HISTNUM 行统计信息以及一个总的概要信息。 |
| -r --relative                                                | 使用相对计时器而不是绝对计时器                               |
| -R --resolution     --secaligned [USEC]                      | 检查时钟分辨率，多次调用clock_gettime()<br/>         clock_gettime() 值列表将会用 -X 报告<br/>将线程唤醒对齐到下一整秒并应用可选的偏移量 |
| -s --system                <br/>--spike=<trigger><br/><br />--spike-nodes=[节点数] | 使用 sys_nanosleep 和 sys_setitimer<br/>记录所有尖峰trigger<br/> 这些是我们可以记录的最大峰值数。<br/>                           如果未指定，默认为 1024 |
| -t --threads                                                 | 每个可用处理器一个线程                                       |
| -t [NUM] --threads=NUM<br/>--tracemark                       | 线程数：没有 NUM，线程 = max_cpus<br/>                           没有 -t 默认值 = 1<br/>超过 -b 延迟时写入跟踪标记 |
| -u --unbuffered                                              | 强制实时处理的无缓冲输出                                     |
| -v --verbose<br/>                                             --dbg_cyclictest | 在标准输出上输出值用于统计<br/>  格式：n:c:v n=tasknum c=count v=value in us  <br />打印对调试 cyclictest 有用的信息 |
| -x --posix_timers                                            | 使用 POSIX 计时器而不是 clock_nanosleep                      |

## 四、测试示例

### 4.1cyclictest在ubuntu的测试示例：

#### 4.1.1 ./cyclictest -a [CPUSET] -t [NUM]  -p PRIO -D --duration的使用

**命令：**

```
./cyclictest -a -t -p99 -D 1m
```

-a: 在哪个cpu上固定线程

-t : 每个可用处理器一个线程

-p: 线程优先级

-D： 指定测试运行长度（运行时间）

**输出结果：**

​	因为此测试电脑含有12个逻辑处理器，有多少个逻辑处理器，就可以开多少个线程，所以在测试过程中共有十二个线程。（可以通过任务管理器-性能查看逻辑处理器数）

```
policy: fifo: loadavg: 0.00 0.00 0.00 1/163 13518

T: 0 (13478) P:99 I:1000 C:  59998 Min:      4 Act:   57 Avg:   78 Max:     426
T: 1 (13479) P:99 I:1500 C:  39998 Min:      5 Act:   49 Avg:   65 Max:     346
T: 2 (13480) P:99 I:2000 C:  29999 Min:      4 Act:   17 Avg:   41 Max:     225
T: 3 (13481) P:99 I:2500 C:  23999 Min:      5 Act:   48 Avg:   50 Max:     363
T: 4 (13482) P:99 I:3000 C:  19999 Min:      4 Act:   15 Avg:   34 Max:     352
T: 5 (13483) P:99 I:3500 C:  17142 Min:     13 Act:   50 Avg:   46 Max:     335
T: 6 (13484) P:99 I:4000 C:  14999 Min:      3 Act:   78 Avg:   59 Max:     332
T: 7 (13485) P:99 I:4500 C:  13332 Min:      4 Act:   17 Avg:   37 Max:     166
T: 8 (13486) P:99 I:5000 C:  11999 Min:      4 Act:   52 Avg:   38 Max:     171
T: 9 (13487) P:99 I:5500 C:  10908 Min:      5 Act:   49 Avg:   66 Max:     347
T:10 (13488) P:99 I:6000 C:   9999 Min:      5 Act:   20 Avg:   48 Max:     345
T:11 (13489) P:99 I:6500 C:   9230 Min:     13 Act:   53 Avg:   63 Max:     326
```

T: 0 序号为0的线程
P: 99 线程优先级为99
C:59998 计数器。线程的时间间隔每达到一次，计数器加1
I: 1000 时间间隔为1000微秒(us)
Min: 最小延时(us)
Act: 最近一次的延时(us)
Avg：平均延时(us)
Max： 最大延时(us)

### 4.2cyclictest在hi3516dv300的测试示例：

#### 4.2.1 ./cyclictest -a [CPUSET] -t [NUM]  -p PRIO -D --duration的使用

**命令：**

```
mkdir dev/shm
chmod 777 shm
./cyclictest -a -t -p99 -D 1m
```

-a: 在哪个cpu上固定线程

-t : 每个可用处理器一个线程

-p: 线程优先级

-D： 指定测试运行长度（运行时间）

**结果：**

```
$ ./cyclictest -a -t -p99 -D 1m
WARN: stat /dev/cpu_dma_latency failed: No such file or directory
WARN: High resolution timers not available
policy: fifo: loadavg: 12.27 11.27 9.24 1/872 18471

T: 0 (18469) P:99 I:1000 C:   6000 Min:   5087 Act: 9077 Avg: 9080 Max:    9196
T: 1 (18470) P:99 I:1500 C:   6000 Min:   4970 Act: 9966 Avg: 9465 Max:   1004
```

T: 0 序号为0的线程
P: 99 线程优先级为99
C: 6000 计数器。线程的时间间隔每达到一次，计数器加1
I: 1500 时间间隔为1500微秒(us)
Min: 最小延时(us)
Act: 最近一次的延时(us)
Avg：平均延时(us)
Max： 最大延时(us)

### 4.3cyclictest在RK3568的测试示例

#### 4.3.1./cyclictest -a [CPUSET] -t [NUM]  -p PRIO -D --duration的使用

**命令：**

```
./cyclictest -a -t -p99 -D 1m
```

**输出结果：**

```
/dev/cpu_dma_latency set to 0us
policy: fifo: loadavg: 1.00 1.07 0.85 2/1185 1724

T: 0 ( 1721) P:99 I:1000 C:  59997 Min:     11 Act:   14 Avg:   13 Max:      57
T: 1 ( 1722) P:99 I:1500 C:  39996 Min:     10 Act:   15 Avg:   15 Max:      54
T: 2 ( 1723) P:99 I:2000 C:  29996 Min:     11 Act:   14 Avg:   15 Max:      50
T: 3 ( 1724) P:99 I:2500 C:  23997 Min:      9 Act:   16 Avg:   15 Max:      38
```

T: 0 序号为0的线程
P: 99 线程优先级为99
C: 59997 计数器。线程的时间间隔每达到一次，计数器加1
I: 1000 时间间隔为1000微秒(us)
Min: 最小延时(us)
Act: 最近一次的延时(us)
Avg：平均延时(us)
Max： 最大延时(us)
